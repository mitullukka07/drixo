@extends('layouts.trainee.master')
        @section('title','Profile')
        @section('content')
        <!-- Start right Content here -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="page-content-wrapper ">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="float-right page-breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Drixo</a></li>
                                        <li class="breadcrumb-item"><a href="#">Traineers</a></li>
                                        <li class="breadcrumb-item active">Profile</li>
                                    </ol>
                                </div>
                                <h5 class="page-title">Profile</h5>
                            </div>
                        </div>
                        <!-- end row -->
                       
                        <div class="row justify-content-center align-items-cente">
                            <div class="col-lg-6">
                                <div class="card m-b-30">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title">Profile</h4>
                                        <p></p>
                                        <form method="POST" action="">
                                            @csrf
                                            
                                            <div class="form-group" style="text-align:center">
                                                <img src="{{$trainee->image}}" width="100px" height="100px">                                                                                
                                            </div>

                                            <div class="form-group">
                                                <label>Name</label>
                                                <div>
                                                    <input type="text" value="{{$trainee->name}}" name="name" class="form-control" placeholder="Enter Name " />
                                                </div>                                               
                                            </div>

                                            <div class="form-group">
                                                <label>Email</label>
                                                <div>
                                                    <input type="text" value="{{$trainee->email}}"  readonly="" name="email" class="form-control" placeholder="Enter Email " />
                                                </div>                                            
                                            </div>

                                            <div class="form-group">
                                                <label>Mobile</label>
                                                <div>
                                                    <input type="text" value="{{$trainee->mobile}}" readonly="" name="mobile" class="form-control" placeholder="Enter Email " />
                                                </div>                                            
                                            </div>

                                            <div class="form-group">
                                                <label>Address</label>
                                                <div>
                                                    <textarea name="address" class="form-control" >{{$trainee->address}}</textarea>
                                                </div>                                            
                                            </div>
                                            
                                            
                                            <div class="form-group">
                                                <div>
                                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                                        Submit
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->
                    </div><!-- container fluid -->
                </div> <!-- Page content Wrapper -->
            </div> <!-- content -->
        </div>
        @endsection



         

