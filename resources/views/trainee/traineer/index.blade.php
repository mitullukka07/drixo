    @extends('layouts.trainee.master')
    @section('content')
    @section('title','TraineerIndex')
    @push('css')
    <style>
        .allerror {
            color: red
        }
    </style>
    @endpush
    <!-- Loader -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="page-content-wrapper ">

                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="float-right page-breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Drixo</a></li>
                                    <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                    <li class="breadcrumb-item active">Datatable</li>
                                </ol>
                            </div>
                            <h5 class="page-title">Traineer List</h5>
                        </div>
                    </div>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Update Traineer</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <form id="editform" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label>Name</label>
                                            <div>
                                                <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control" placeholder="Enter Name " />
                                            </div>
                                            <span class="text-danger" id="name"></span>
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label>
                                            <div>
                                                <input type="text" name="email" id="email" value="{{old('email')}}" class="form-control" placeholder="Enter Email " />
                                            </div>
                                            <span class="text-danger" id="email"></span>
                                        </div>

                                        <div class="form-group">
                                            <label>Mobile</label>
                                            <div>
                                                <input type="text" name="mobile" id="mobile" value="{{old('mobile')}}" class="form-control" placeholder="Enter Mobile " />
                                            </div>
                                            <span class="text-danger" id="mobile"></span>
                                        </div>

                                        <div class="form-group">
                                            <label>Image</label>
                                            <div>
                                                <input type="file" name="image" id="image" value="{{old('image')}}" class="form-control" />
                                            </div>
                                            <span class="text-danger" id="imageError"></span>
                                            <!-- <img id="upload-img" height="80px" width="80px" class="img-thumbnail"> -->
                                            <div id="userimage"></div>
                                            <span class="text-danger" id="image"></span>
                                        </div>

                                        <div class="form-group">
                                            <label>Textarea</label>
                                            <div>
                                                <textarea required name="address" id="address" value="{{old('address')}}" class="form-control" rows="3"></textarea>
                                            </div>
                                            <span class="text-danger" id="address"></span>
                                        </div>
                                        @php
                                        $deptData=DB::table('departments')->get();
                                        @endphp

                                        <div class="form-group">
                                            <label>Department</label>
                                            <div>
                                                <select class="custom-select" id="department_id" name="department_id">
                                                    <option selected>Select Department</option>
                                                    @foreach($deptData as $value)
                                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                </div>
                                <input type="hidden" id="id" name="id">
                                </form>
                                <div class="modal-footer">
                                    <button type="submit" id="update" class="btn btn-primary" data-dismiss="modal">Update</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="myModalshow" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">View Traineer</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <table class="table table-borderd">
                                    <div class="modal-body">
                                        <form id="showform" enctype="multipart/form-data">
                                            @csrf
                                            <tr>
                                                <th><b>Name</b></th>
                                                <td><span id="set_name"></span></td>
                                            </tr>

                                            <tr>
                                                <th><b>Email</b></th>
                                                <td><span id="set_email"></span></td>
                                            </tr>

                                            <tr>
                                                <th><b>Mobile</b></th>
                                                <td><span id="set_mobile"></span></td>
                                            </tr>

                                            <tr>
                                                <th><b>Image</b></th>
                                                <td><img src="" id="set_image" width="50px" height="50px"></img></td>
                                            </tr>


                                            <tr>
                                                <th><b>Address</b></th>
                                                <td><span id="set_address"></span></td>
                                            </tr>

                                            <tr>
                                                <th><b>Department</b></th>
                                                <td><span id="set_department"></span></td>
                                            </tr>
                                    </div>
                                </table>
                                <input type="hidden" id="id" name="id">
                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end row -->
                   
                    <div class="">
                        {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                    </div> <!-- end row -->

                </div><!-- container fluid -->
            </div> <!-- Page content Wrapper -->
        </div> <!-- content -->
        <!-- End Right content here -->
    </div>
    <!-- END wrapper -->

    @endsection

    @push('js')
    {!! $dataTable->scripts() !!}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).on("click", ".editbtn", function(e) {
            e.preventDefault();

            var id = $(this).attr("href");
            $.ajax({
                url: id,
                type: 'GET',
                dataType: "JSON",
                success: function(data) {

                    $("#id").val(data.traineer.id);
                    $("#name").val(data.traineer.name);
                    $("#email").val(data.traineer.email);
                    $("#mobile").val(data.traineer.mobile);
                    $("#userimage").html('<img src="' + data.traineer.image + '" height="50px" width="50px">');
                    $("#address").val(data.traineer.address);

                    $("#department_id").val(data.department);

                },
                error: function(data) {
                    console.log(data);
                }
            });
        });


        //View data
        $(document).on('click', '.btnshow', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    $("#set_name").text(data.traineer.name);
                    $("#set_email").text(data.traineer.email);
                    $("#set_mobile").text(data.traineer.mobile);
                    $("#set_image").attr('src', data.traineer.image);
                    $("#set_address").text(data.traineer.address);
                    $("#set_department").text(data.department);
                }
            });
        })

        //Update data
        $('#update').click(function(e) {

            e.preventDefault();
            var form = $('#editform')[0];
            var frm_data = new FormData(form);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("admin.traineer.update")}}',
                type: "post",
                data: frm_data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function(data) {
                    swal({
                        title: "Updated",
                        text: "update Successfully",
                        buttons: ['Cancel', 'Update']
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            swal("Good job!", "Update Data Successfully!", "success")
                            window.LaravelDataTables['traineers-table'].draw();
                            $("#myModal").modal('hide');
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
                },
                error: function(data) {
                    $.each(data.responseJSON.errors, function(key, value) {
                        $('input[name=' + key + ']').after('<span class="allerror">' + value + '</span>')
                    });
                }
            });

            $('#myModal').on('hidden.bs.modal', function() {
                $(".allerror").empty();
            });
            return false;
        })
    </script>
    @endpush