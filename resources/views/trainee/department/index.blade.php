    @extends('layouts.trainee.master')
    @section('content')
    @section('title','Index')
    @push('css')
    <style>
        .allerror {
            color: red;
        }

        body.modal-open {
            overflow: hidden;
            position: fixed;
        }
    </style>
    @endpush
    <!-- Loader -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <!-- Top Bar Start -->

            <!-- Top Bar End -->

            <div class="page-content-wrapper ">

                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="float-right page-breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Drixo</a></li>
                                    <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                    <li class="breadcrumb-item active">Department</li>
                                </ol>
                            </div>
                            <h5 class="page-title">Department List</h5>
                        </div>
                    </div>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Update Department</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <form id="editform" enctype='multipart/form-data'>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <div>
                                                <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control" placeholder="Enter Name " />
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label>Image</label>
                                            <div>
                                                <input type="file" name="image" id="image" class="form-control" />
                                            </div>
                                            <!-- <img id="upload-img" height="80px" width="80px" class="img-thumbnail"> -->
                                            <div id="userimage"></div>
                                        </div>


                                    </div>
                                    <input type="hidden" id="id" name="id">

                                    <div class="modal-footer">
                                        <button type="submit" id="update" class="btn btn-primary">Update</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                    <div id="myModalshow" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">View Department</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <form id="showform" enctype='multipart/form-data'>
                                    <table class="table table-border">
                                        <div class="modal-body">
                                            <tr>
                                                <th><b>Name:</b></th>
                                                <td><span id="set_name"></span></td>
                                            </tr>

                                            <tr>
                                                <th><b>Image:</b></th>
                                                <td><img src="" id="set_image" width="50px" height="50px"></img></td>
                                            </tr>

                                        </div>

                                        <input type="hidden" id="id" name="id">
                                    </table>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    <!-- end row -->
                   
                    <div class="">
                        {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                    </div> <!-- end row -->

                </div><!-- container fluid -->

            </div> <!-- Page content Wrapper -->

        </div> <!-- content -->


        <!-- End Right content here -->

    </div>
    <!-- END wrapper -->

    @endsection

    @push('js')
    {!! $dataTable->scripts() !!}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).on('click', '.btnedit', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                url: url,
                type: "GET",
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function(data) {
                    $("#id").val(data.id);
                    $("#name").val(data.name);
                    $("#userimage").html('<img src="' + data.image + '" width="50px" height="50px">');
                },

            });
        });

        $(document).on('click', '.btnshow', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                url: url,
                type: "GET",
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function(data) {
                    $("#set_name").text(data.name);
                    $("#set_image").attr('src', data.image);
                }
            });
        });

        $(document).on('submit', "#editform", function(e) {
            e.preventDefault();
            // var myform = $('#editform')[0];
            var dep_data = new FormData(this);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("admin.department.update")}}',
                type: "post",
                data: dep_data,
                cache: false,
                contentType: false,
                processData: false,

                success: function(data) {
                    swal({
                        title: 'Updated',
                        text: 'Updated Succesfully',
                        buttons: ['Cancel', 'Update']
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            window.LaravelDataTables['departments-table'].draw();
                            swal("Good job!", "Update Data Successfully!", "success")
                            $("#myModal").modal('hide');
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
                },
                error: function(data) {
                    $.each(data.responseJSON.errors, function(key, value) {
                        $('input[name=' + key + ']').after('<span class="allerror">' + value + '</span>');
                    });
                }
            });

            return false;
        });
    </script>

    @endpush