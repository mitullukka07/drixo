    @extends('layouts.trainee.master')
    @section('content')
    @section('title','TraineeIndex')
    @push('css')

    <style>
        .allerror {
            color: red;
        }

        td .badge-danger {
            cursor: pointer;
        }

        td {
            overflow: hidden;
            width: 380px;
        }

        table.dataTable.nowrap th,
        table.dataTable.nowrap td {
            white-space: normal !important;
        }

        td span .status {
            text-align: center;
        }

        body.modal-open {
            overflow: hidden;
        }
    </style>
    @endpush
    <!-- Loader -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="page-content-wrapper ">

                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="float-right page-breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Drixo</a></li>
                                    <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                    <li class="breadcrumb-item active">Datatable</li>
                                </ol>
                            </div>
                            <h5 class="page-title">Task List</h5>
                        </div>
                    </div>

                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Update Task</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <form id="editform" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label>Title</label>
                                            <div>
                                                <input type="text" name="title" id="title" value="{{old('.title')}}" class="form-control" placeholder="Enter Name " />
                                            </div>
                                            <span class="text-danger" id="titleError"></span>
                                        </div>

                                        <div class="form-group">
                                            <label>Descreption(Optional)</label>
                                            <div>
                                                <textarea class="form-control" id="descrption" name="descrption" rows="3"></textarea>
                                            </div>
                                            <span class="text-danger" id="descreptionError"></span>
                                        </div>

                                        @php
                                        $traineerData = DB::table('traineers')->get();
                                        @endphp
                                        <div class="form-group">
                                            <label>Assinging</label>
                                            <div>
                                                <select class="custom-select" name="traineer_id" id="traineer_id">
                                                    <option selected>Select</option>
                                                    @foreach($traineerData as $value)
                                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <span class="text-danger" id="traineerError"></span>
                                        </div>
                                        @php
                                        $traineeData = DB::table('trainees')->get();
                                        @endphp
                                        <div class="form-group">
                                            <label>To Assign</label>
                                            <div>
                                                <select class="custom-select multi" multiple name="trainee_id[]" id="trainee_id">
                                                    @foreach($traineeData as $value)
                                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Image</label>
                                            <div>
                                                <input type="file" name="image[]" multiple class="form-control" />
                                            </div>
                                            <input type="hidden" id="updateimage" name="updateimage" value="">
                                            <div class="userimage" width="30px" height="30px">

                                            </div>
                                            <span class="text-danger" id="imageError"></span>
                                        </div>

                                        <div class="form-group">
                                            <label>Start Date</label>
                                            <div>
                                                <input type="text" name="start_date" id="start_date" class="form-control" placeholder="Enter date " />
                                            </div>
                                            <span class="text-danger" id="startdateError"></span>
                                        </div>


                                        <div class="form-group">
                                            <label>End Date</label>
                                            <div>
                                                <input type="text" name="end_date" id="end_date" class="form-control" placeholder="Enter date" />
                                            </div>
                                            <span class="text-danger" id="enddateError"></span>
                                        </div>

                                        <div class="form-group">
                                            <label>Status</label>
                                            <div>
                                                <select class="custom-select" id="status1" name="status">
                                                    <option>Select</option>
                                                    <option value="1" id="statusactive">Active</option>
                                                    <option value="2" id="statusdeactive">Deactive</option>
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" id="id" name="id">
                                    </form>
                                </div>

                                <div class="modal-footer">
                                    <button type="submit" id="update" class="btn btn-primary" data-dismiss="modal">Update</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="myModalshow" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">View Task</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <table class="table table-border">
                                    <div class="modal-body">
                                        <form id="showform" enctype="multipart/form-data">
                                            @csrf

                                            <tr>
                                                <th><b>Title</b></th>
                                                <td><span id="set_title"></span></td>
                                            </tr>

                                            <tr>
                                                <th><b>Descreption</b></th>
                                                <td><span id="set_descrption"></span></td>
                                            </tr>

                                            <tr>
                                                <th><b>Assigning</b></th>
                                                <td><span id="set_assigning"></span></td>
                                            </tr>

                                            <tr>
                                                <th><b>To Assign</b></th>
                                                <td><span id="set_to_assign"></span></td>
                                            </tr>

                                            <tr>
                                                <th><b>Image</b></th>
                                                <td>
                                                    <div id="set_image"></div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th><b>StartDate</b></th>
                                                <td><span id="set_start_date"></span></td>
                                            </tr>

                                            <tr>
                                                <th><b>EndDate</b></th>
                                                <td><span id="set_end_date"></span></td>
                                            </tr>

                                            <tr>
                                                <th><b>Status</b></th>
                                                <td><span id="set_status"></span></td>
                                            </tr>
                                    </div>
                                </table>
                                <input type="hidden" id="id" name="id">
                                </form>
                                <div class="modal-footer">

                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- end row -->

                    <div class="">
                        {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                    </div> <!-- end row -->

                </div><!-- container fluid -->
            </div> <!-- Page content Wrapper -->
        </div> <!-- content -->
        <!-- End Right content here -->
    </div>
    <!-- END wrapper -->
    @endsection

    @push('js')
    {!! $dataTable->scripts() !!}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.multi').select2();
        });
    </script>
    <script>
        $(document).on("click", '.btnshow', function(e) {
            e.preventDefault();
            var url = $(this).attr("href");
            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    $("#set_title").text(data.task.title);
                    $("#set_descrption").text(data.task.descrption);
                    $("#set_assigning").text(data.traineer);

                    var str = data.trainee;
                    var select = str.split(" ");

                    var b = "";
                    for (var i = 0; i < select.length; i++) {
                        b += '<p class="badge badge-danger">' + select[i] + '</p>';
                    }
                    $("#set_to_assign").html(b);

                    var src = data.task.image;
                    var a = src.split(",");
                    var b = '';
                    for (var i = 0; i < a.length; i++) {
                        b += '<img src="{{asset("uploads")}}' + '/' + '' + a[i] + '" height="50px" width="50px">';
                    }
                    $('#set_image').html(b);
                    $("#set_start_date").text(data.task.start_date);
                    $("#set_end_date").text(data.task.end_date);

                    if (data.task.status == 1) {
                        var status = '<span class="badge badge-primary ">Active</span>';
                    } else {
                        var status = '<span class="badge badge-danger">Deactive</span>';
                    }
                    $("#set_status").html(status);
                }
            });
        });


        //Edit data
        $(document).on("click", '.editbtn', function(e) {
            e.preventDefault();
            var url = $(this).attr("href");
            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                success: function(data) {

                    $('#id').val(data.task.id);
                    $("#title").val(data.task.title);
                    $("#descrption").val(data.task.descrption);
                    $("#traineer_id").val(data.traineer);
                    var selectid = [];
                    $.each(data.trainee, function(key, value) {
                        selectid.push(value.id)
                    })
                    $("#trainee_id").val(selectid).change();


                    var src = data.task.image;
                    var a = src.split(",");
                    var b = '';
                    for (var i = 0; i < a.length; i++) {
                        b += '<img src="{{asset("uploads")}}' + '/' + '' + a[i] + '" height="50px" width="50px">';
                    }
                    $('.userimage').html(b);
                    $("#start_date").val(data.task.start_date);
                    $("#end_date").val(data.task.end_date);
                    if (data.task.status == '1') {
                        $("#statusactive").prop('selected', 'selected');
                    } else {
                        $("#statusdeactive").prop('selected', 'selected');
                    }
                }
            });
        });

        $("#update").click(function(e) {
            e.preventDefault();
            var form = $("#editform")[0];
            var data = new FormData(form);
            $.ajax({
                url: '{{route("admin.task.update")}}',
                data: data,
                type: 'post',
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function(data) {
                    swal({
                        title: 'Updated',
                        text: 'Updated Succesfully',
                        buttons: ['Cancel', 'Update']
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            swal('Good Job!', "Update Data Successfully!", "success");
                            window.LaravelDataTables['task-table'].draw();
                            $("#myModal").modal('hide');
                        }
                    });
                },
                error: function(data) {
                    $.each(data.responseJSON.errors, function(key, value) {
                        $('input[name=' + key + ']').after('<span class="allerror">' + value + '</span>')
                        // $('textarea[name='+ key +']').after('<span class="allerror">'+ value +'</span>')
                    });
                }
            });
            $("#myModal").on('hidden.bs.modal', function() {
                $('.allerror').empty();
            });
            return false;
        })

        $(document).on("click", ".status", function(e) {
            e.preventDefault();
            var id = $(this).attr("data-id");
            var status = $(this).attr("data-status");

            $.ajax({
                type: "GET",
                dataType: "JSON",
                url: "{{route('admin.task.changeStatus')}}",
                data: {
                    'status': status,
                    'id': id
                },
                success: function(data) {
                    swal({
                        title: 'Are you sure Active',
                        text: 'Updated Succesfully',
                        buttons: ['Cancel', 'Update']
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            window.LaravelDataTables['task-table'].draw();
                        }
                    });
                }
            });
        });
                                
    </script>
    @endpush