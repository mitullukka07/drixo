@component('mail::message')
# Introduction

Your Login password.
{{$trainee->password}}
@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
