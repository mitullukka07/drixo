@extends('layouts.traineer.master')
@section('title','Change Password')
@section('content')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="page-content-wrapper ">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="float-right page-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Drixo</a></li>
                                <li class="breadcrumb-item"><a href="#">Admin</a></li>
                                <li class="breadcrumb-item active">Form Validation</li>
                            </ol>
                        </div>
                        <h5 class="page-title">Change password</h5>
                    </div>
                </div>
                <!-- end row -->
                @if($message = Session::get('error'))
                <div class="alert alert-danger"  class="close" id="success">
                    {{$message}}
                </div>
                @endif

                @if($message = Session::get('update'))
                <div class="alert alert-primary" class="close" id="success">
                    {{$message}}
                </div>
                @endif

                @if($message = Session::get('message'))
                <div class="alert alert-primary" class="close" id="success">
                    {{$message}}
                </div>
                @endif
                <div class="row justify-content-center align-items-cente">
                    <div class="col-lg-6">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Change password</h4>
                                <p></p>
                                <form method="POST" action="">
                                    @csrf
                                    <div class="form-group">
                                        <label>Old Password</label>
                                        <div>
                                            <input type="password" name="oldpassword" class="form-control" placeholder="Enter Password " />
                                        </div>
                                        <span style="color:red">
                                            @error('oldpassword')
                                            {{$message}}
                                            @enderror
                                        </span>
                                    </div>

                                    <div class="form-group">
                                        <label>New Password</label>
                                        <div>
                                            <input type="password" name="newpassword" class="form-control" placeholder="Enter New Password " />
                                        </div>
                                        <span style="color:red">
                                            @error('newpassword')
                                            {{$message}}
                                            @enderror
                                        </span>
                                    </div>

                                    <div class="form-group">
                                        <div>
                                            <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div><!-- container fluid -->
        </div> <!-- Page content Wrapper -->
    </div> <!-- content -->
</div>
@endsection

@push('js')

@endpush