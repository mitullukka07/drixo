<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
    <div style="margin:50px auto;width:70%;padding:20px 0">
        <div style="border-bottom:1px solid #eee">
            <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">Your Task</a>
        </div>
        <p style="font-size:1.1em">Hello,</p>
        <p><span>Title:</span>{{$title->title}}</p>
        <p><h3>Descreption:</h3></br>{{$title->descrption}}</p>

       
        <p><span>Assigning:</span>{{$title->name}}</p>
        <p><span>Start Date:</span>{{$title->start_date}}</p>
        <p><span>End Date:</span>{{$title->end_date}}</p>
        <p>Assignment task</p>
        <h2 style="background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;"></h2>
        <p style="font-size:0.9em;">Regards,<br />Your Brand</p>
        <hr style="border:none;border-top:1px solid #eee" />
        <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
            <p>California</p>
        </div>
    </div>
</div>