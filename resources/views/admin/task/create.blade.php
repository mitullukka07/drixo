@extends('layouts.admin.master')
@section('title','Create')
@section('content')
@push('css')
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" /> -->

<style>
    .text-danger {
        color: red !important;
    }

    .multi {
        width: 100% !important;
        color: none;
    }
</style>

@endpush
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="page-content-wrapper ">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="float-right page-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Drixo</a></li>
                                <li class="breadcrumb-item"><a href="#">Admin</a></li>
                                <li class="breadcrumb-item active">Form Validation</li>
                            </ol>
                        </div>
                        <h5 class="page-title">Add Trainee</h5>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Trainee</h4>
                                <p></p>
                                <form method="POST" action="" id="myform" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label>Title</label>
                                        <div>
                                            <input type="title" name="title" class="form-control" placeholder="Enter Name " />
                                        </div>
                                        <span class="text-danger" id="titleError"></span>
                                    </div>

                                    <div class="form-group">
                                        <label>Descrpetion</label>
                                        <div>
                                            <textarea required class="form-control" name="descrption" rows="3"></textarea>
                                        </div>
                                        <span class="text-danger" id="descreptionError"></span>
                                    </div>
                                    
                                   
                                    <div class="form-group">
                                        <label>Assinging</label>
                                        <div>
                                            <select class="custom-select" name="traineer_id">
                                                <option selected>Select</option>
                                                @foreach($traineer as $tra)
                                                <option value="{{$tra->id}}">{{$tra->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <span class="text-danger" id="traineerError"></span>
                                    </div>

                                    <div class="form-group">
                                        <label>To Assign</label>
                                        <div>
                                            <select class="custom-select multi" multiple name="trainee_id[]">
                                                @foreach($trainee as $trai)
                                                <option value="{{$trai->id}}">{{$trai->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Image</label>
                                        <div>
                                            <input type="file" name="image[]" multiple class="form-control" />
                                        </div>
                                        <span class="text-danger" id="imageError"></span>
                                    </div>

                                    <div class="form-group">
                                        <label>Start Date</label>
                                        <div>
                                            <input type="text" name="start_date" id="startdate" class="form-control" placeholder="Enter date " />
                                        </div>
                                        <span class="text-danger" id="startdateError"></span>
                                    </div>


                                    <div class="form-group">
                                        <label>End Date</label>
                                        <div>
                                            <input type="text" name="end_date" id="enddate" class="form-control" placeholder="Enter date" />
                                        </div>
                                        <span class="text-danger" id="enddateError"></span>
                                    </div>


                                    <div class="form-group">
                                        <label>Status</label>
                                        <div>
                                            <select class="custom-select" name="status">
                                                <option>Select</option>
                                                <option value="1">Active</option>
                                                <option value="0" selected>Deactive</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div>
                                            <button type="submit" name="submit" id="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                    <!-- </form> -->
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div><!-- container fluid -->
        </div> <!-- Page content Wrapper -->
    </div> <!-- content -->
</div>
@endsection
@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script> -->

<script>
    $("#startdate").datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy/mm/dd',
        startDate: '-0d'
    });

    $('#enddate').datepicker({
        format: 'yyyy/mm/dd',
        autoclose: true,
        todayHighlight: true,
        startDate: '-0d'
    });

    $(document).ready(function() {
        $('.multi').select2();
    });

    $(document).ready(function() {
        $("#submit").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);
            $.ajax({
                url: "{{route('admin.task.store')}}",
                data: data,
                type: "post",
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function(data) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        buttons: ["Cancel", "Submit"]
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            window.location.href = "../task/index";
                        }
                    });
                },
                error: function(data) {
                    $(".text-danger").text("");
                    $("#titleError").text(data.responseJSON.errors.title);
                    $("#descreptionError").text(data.responseJSON.errors.descreption);
                    $("#traineerError").text(data.responseJSON.errors.traineer_id);
                    $("#imageError").text(data.responseJSON.errors.image);
                    $("#startdateError").text(data.responseJSON.errors.start_date);
                }

            })
        });
    })
</script>
@endpush


