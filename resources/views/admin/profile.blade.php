        @extends('layouts.admin.master')
        @section('title','Profile')
        @section('content')
        <!-- Start right Content here -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="page-content-wrapper ">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="float-right page-breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Drixo</a></li>
                                        <li class="breadcrumb-item"><a href="#">Traineers</a></li>
                                        <li class="breadcrumb-item active">Profile</li>
                                    </ol>
                                </div>
                                <h5 class="page-title">Profile</h5>
                            </div>
                        </div>
                        <!-- end row -->
                        @if($message = Session::get('update'))
                        <div class="alert alert-primary" class="close" id="success">
                            {{$message}}
                        </div>
                        @endif
                        <div class="row justify-content-center align-items-cente">
                            <div class="col-lg-6">
                                <div class="card m-b-30">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title">Profile</h4>
                                        <p></p>
                                        <form method="post" action="">
                                            @csrf                                            
                                            <div class="form-group">
                                                <label>Name</label>
                                                <div>
                                                    <input type="text" value="{{$admin->name}}" name="name" class="form-control" placeholder="Enter Name " />
                                                </div>                                               
                                            </div>

                                            <div class="form-group">
                                                <label>Email</label>
                                                <div>
                                                    <input type="text" value="{{$admin->email}}"  readonly="" name="email" class="form-control" placeholder="Enter Email " />
                                                </div>                                            
                                            </div>
                                                                                                                                  
                                            <div class="form-group">
                                                <div>
                                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">
                                                        Update
                                                    </button>                                                    
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->
                    </div><!-- container fluid -->
                </div> <!-- Page content Wrapper -->
            </div> <!-- content -->
        </div>
        @endsection       

