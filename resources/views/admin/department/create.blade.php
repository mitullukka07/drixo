@extends('layouts.admin.master')
@section('title','Create')
@push('css')
<style>
    .text-danger {
        color: red !important;
    }
</style>
@endpush
@section('content')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="page-content-wrapper ">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="float-right page-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Drixo</a></li>
                                <li class="breadcrumb-item"><a href="#">Admin</a></li>
                                <li class="breadcrumb-item active">Form Validation</li>
                            </ol>
                        </div>
                        <h5 class="page-title">Add Department</h5>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Department</h4>
                                <p></p>
                                <!-- <form method="POST" action="" id="myform" enctype="multipart/form-data"> -->
                                {{ Form::open(['route'=>'admin.department.store','id'=>'myform','action'=>'','method'=>'post','enctype'=>'multipart/form-data']) }}
                                @csrf
                                <!-- <div class="form-group">
                                        <label>Name</label>
                                        <div>
                                            <input type="text" name="name" class="form-control" placeholder="Enter Name " />
                                        </div>
                                        <span class="text-danger" id="nameError"></span>
                                    </div> -->

                                {{Form::label('Name','Name')}}
                                {{Form::text('name',null,array('Placeholder'=>'Enter Name','class'=>'form-control'))}}
                                <span class="text-danger" id="nameError"></span></br>

                                <!-- <div class="form-group">
                                        <label>Image</label>
                                        <div>
                                            <input type="file" name="image" class="form-control" />
                                        </div>
                                        <span class="text-danger" id="imageError"></span>
                                    </div> -->

                                {{Form::label('image','image')}}
                                {{Form::file('image',['id'=>'image','class'=>'form-control'])}}
                                <span class="text-danger" id="imageError"></span></br>

                                <div class="form-group">
                                    <div>
                                        <!-- <button type="submit" name="submit" id="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button> -->
                                        {{ Form::button('submit',array('name'=>'submit','id'=>'submit','class'=>'btn btn-primary')) }}
                                        <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                                <!-- </form> -->
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div><!-- container fluid -->
        </div> <!-- Page content Wrapper -->
    </div> <!-- content -->
</div>
@endsection
@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#submit').click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route("admin.department.store") }}',
                type: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function(result) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        buttons: ["Cancel", "Submit"]
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $('#myform')[0].reset();
                            window.location.href = "../department/index";
                        } else {
                            swal("Cancelled", "", "error")
                        }
                    });
                },
                error: function(result) {
                    $('#nameError').text(result.responseJSON.errors.name);
                    $('#imageError').text(result.responseJSON.errors.image);
                }
            });

        });
    });
</script>
@endpush