@extends('layouts.admin.master')
@section('title','Create')
@push('css')
<style>
    .text-danger {
        color: red !important;
    }
</style>
@endpush
@section('content')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="page-content-wrapper ">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="float-right page-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Drixo</a></li>
                                <li class="breadcrumb-item"><a href="#">Admin</a></li>
                                <li class="breadcrumb-item active">Form Validation</li>
                            </ol>
                        </div>
                        <h5 class="page-title">Add Trainee</h5>
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Trainee</h4>
                                <p></p>
                                {{Form::open(['route'=>'admin.trainee.store','id'=>'myform','method'=>'post','enctype'=>'multipart/form-data'])}}
                                <!-- <form method="POST" action="" id="myform" enctype="multipart/form-data"> -->
                                @csrf


                                {{ Form::label('Name','Name')  }}
                                {{ Form::text('name',null,['placeholder'=>'Enter Name','class'=>'form-control'])   }}
                                <span class="text-danger" id="nameError"></span></br>

                                {{ Form::label('Email','Email')    }}
                                {{ Form::email('email',null,['placeholder'=>'Enter Email','class'=>'form-control'])    }}
                                <span class="text-danger" id="emailError"></span></br>

                                {{ Form::label('Mobile','Mobile')  }}
                                {{ Form::text('mobile',null,['placeholder'=>'Enter Mobile','class'=>'form-control'])   }}
                                <span class="text-danger" id="mobileError"></span></br>

                                {{ Form::label('Profile Image','Profile Image')}}
                                {{ Form::file('image',['class'=>'form-control'])}}
                                <span class="text-danger" id="imageError"></span></br>

                                {{ Form::label('Doj','Doj')}}
                                {{ Form::text('doj',null,['placeholder'=>'Select date','class'=>'form-control','id'=>'doj'])}}
                                <span class="text-danger" id="dojError"></span></br>

                                {{ Form::label('Address','Address')}}
                                {{ Form::textarea('address',null,['class'=>'form-control','rows'=>'3'])}}
                                <span class="text-danger" id="addressError"></span></br>

                                {{ Form::label('Department','Department')}}
                                {{ Form::select('department_id',$select,null,['class'=>'form-control'])}}
                                </br>
                                <div class="form-group">
                                    <div>
                                        <!-- <button type="submit" name="submit" id="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button> -->

                                        {{ Form::button('submit',array('name'=>'submit','id'=>'submit','class'=>'btn btn-primary')) }}
                                        <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                                <!-- </form> -->
                                {{ Form::close() }}

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div><!-- container fluid -->
        </div> <!-- Page content Wrapper -->
    </div> <!-- content -->
</div>
@endsection
@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $("#submit").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);

            $.ajax({
                url: "{{ route('admin.trainee.store') }}",
                type: "post",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function(result) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        buttons: ["Cancel", "Submit"]
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $("#myform")[0].reset();
                            window.location.href = "../trainee/index";
                        } else {
                            swal("Cancelled", "", "error")
                        }
                    });
                },
                error: function(result) {
                    
                    $('#nameError').text(result.responseJSON.errors.name);
                    $('#emailError').text(result.responseJSON.errors.email);
                    $('#mobileError').text(result.responseJSON.errors.mobile);
                    $('#imageError').text(result.responseJSON.errors.image);
                    $('#dojError').text(result.responseJSON.errors.doj);
                    $('#addressError').text(result.responseJSON.errors.address);
                }
            });

        });

        $(":file").change(function() {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });

        function imageIsLoaded(e) {
            $("#upload-img").attr('src', e.target.result);
        }

        $("#doj").datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy/mm/dd',
            endDate: '0d'
        });
    });
</script>
@endpush