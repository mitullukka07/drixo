@extends('layouts.admin.master')
@section('title','Create')
@section('content')
@push('css')
<style>
    .text-danger {
        color: red !important;
    }
</style>
@endpush
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="page-content-wrapper ">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="float-right page-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Drixo</a></li>
                                <li class="breadcrumb-item"><a href="#">Admin</a></li>
                                <li class="breadcrumb-item active">Form Validation</li>
                            </ol>
                        </div>
                        <h5 class="page-title">Add Traineer</h5>
                    </div>
                </div>
                <!-- end row -->
                @if($message = Session::get('error'))
                <div class="alert alert-primary" class="close" id="success">
                    {{$message}}
                </div>
                @endif

                @if($message = Session::get('update'))
                <div class="alert alert-primary" class="close" id="success">
                    {{$message}}
                </div>
                @endif

                @if($message = Session::get('message'))
                <div class="alert alert-primary" class="close" id="success">
                    {{$message}}
                </div>
                @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Traineer</h4>
                                <p></p>
                                {{ Form::open(['route'=>'admin.traineer.store','id'=>'myform','method'=>'post','enctype'=>'multipart/form-data'])  }}
                                <!-- <form method="POST" action="" id="myform" enctype="multipart/form-data"> -->
                                @csrf
                                <!-- <div class="form-group">
                                        <label>Name</label>
                                        <div>
                                            <input type="text" name="name" class="form-control" placeholder="Enter Name " />
                                        </div>
                                        <span class="text-danger" id="nameError"></span>
                                    </div> -->

                                {{ Form::label('Name') }}
                                {{ Form::text('name',null,['placeholder'=>'Enter Name','class'=>'form-control'])   }}
                                <span class="text-danger" id="nameError"></span></br>

                                <!-- <div class="form-group">
                                        <label>Email</label>
                                        <div>
                                            <input type="text" name="email" class="form-control" placeholder="Enter Email " />
                                        </div>
                                        <span class="text-danger" id="emailError"></span>
                                    </div> -->

                                {{ Form::label('Email')    }}
                                {{ Form::email('email',null,['placeholder'=>'Enter Email','class'=>'form-control'])   }}
                                <span class="text-danger" id="emailError"></span></br>

                                <!-- <div class="form-group">
                                        <label>Mobile</label>
                                        <div>
                                            <input type="text" name="mobile" class="form-control" placeholder="Enter Mobile " />
                                        </div>
                                        <span class="text-danger" id="mobileError"></span>
                                    </div> -->

                                {{ Form::label('Mobile')    }}
                                {{ Form::text('mobile',null,['placeholder'=>'Enter Mobile','class'=>'form-control'])   }}
                                <span class="text-danger" id="mobileError"></span></br>

                                <!-- <div class="form-group">
                                        <label>Image</label>
                                        <div>
                                            <input type="file" name="image" class="form-control" />
                                        </div>
                                        <span class="text-danger" id="imageError"></span>
                                        <img id="upload-img" height="80px" width="80px" class="img-thumbnail">
                                    </div> -->

                                {{ Form::label('Profile Image')    }}
                                {{ Form::file('image',['class'=>'form-control'])   }}
                                <span class="text-danger" id="imageError"></span></br>

                                <!-- <div class="form-group">
                                        <label>Textarea</label>
                                        <div>
                                            <textarea required name="address" class="form-control" rows="3"></textarea>
                                        </div>
                                        <span class="text-danger" id="addressError"></span>
                                    </div> -->

                                {{ Form::label('Address')    }}
                                {{ Form::textarea('address',null,['rows'=>'3','class'=>'form-control'])   }}
                                <span class="text-danger" id="addressError"></span></br>


                                {{ Form::label('Department','Department')}}
                                {{ Form::select('department_id',$select,null,['class'=>'form-control'])}}
                                </br>

                                <div class="form-group">
                                    <div>
                                        <!-- <button type="submit" name="submit" id="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button> -->
                                        {{ Form::button('submit',array('name'=>'submit','id'=>'submit','class'=>'btn btn-primary')) }}
                                        <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                                <!-- </form> -->
                                {{ Form::close()   }}
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div><!-- container fluid -->
        </div> <!-- Page content Wrapper -->
    </div> <!-- content -->
</div>
@endsection
@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#submit').click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);

            $.ajax({
                url: "{{ route('admin.traineer.store') }}",
                type: "post",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                success: function(result) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        buttons: ['Cancel', 'Submit']
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $("#myform")[0].reset();
                            window.location.href = "../traineer/index";
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
                },
                error: function(result) {
                    $(".text-danger").text("");
                    $("#nameError").text(result.responseJSON.errors.name);
                    $("#emailError").text(result.responseJSON.errors.email);
                    $("#mobileError").text(result.responseJSON.errors.mobile);
                    $("#imageError").text(result.responseJSON.errors.image);
                    $("#addressError").text(result.responseJSON.errors.address);
                }
            });
        });

        //Image upload before preview
        $(":file").change(function() {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });

        function imageIsLoaded(e) {
            $("#upload-img").attr('src', e.target.result);
        }
    });
</script>
@endpush