<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="ion-close"></i>
    </button>

    <div class="left-side-logo d-block d-lg-none">
        <div class="text-center">
            <a href="index.html" class="logo"><img src="{{asset('assets/images/logo-dark.png')}}" height="20" alt="logo"></a>
        </div>
    </div>

    <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
            <ul>
                <li>
                    <a href="{{route('traineers.traineerdashboard')}}" class="waves-effect">
                        <i class="dripicons-meter"></i>
                        <span> Dashboard </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('traineers.trainee.index')}}" class="waves-effect">
                        <i class="dripicons-user"></i>
                        <span> Trainee </span>
                    </a>
                </li>
                              
                <li>
                    <a href="{{route('traineers.traineer.index')}}" class="waves-effect">
                        <i class="dripicons-user"></i>
                        <span> Trainer </span>
                    </a>
                </li>
                
                <li>
                    <a href="{{route('traineers.task.index')}}" class="waves-effect">
                        <i class="fa fa-tasks"></i>
                        <span> Task </span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
</div>

