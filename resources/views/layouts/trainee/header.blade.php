<div class="topbar">

    <div class="topbar-left	d-none d-lg-block">
        <div class="text-center">

            <a href="index.html" class="logo"><img src="{{asset('assets/images/logo.png')}}" height="20" alt="logo"></a>
        </div>
    </div>

    <nav class="navbar-custom">
        <ul class="list-inline float-right mb-0">
            <li class="list-inline-item dropdown notification-list" style="color:#fff">
               
                    Hello,{{ Auth::guard('trainee')->user()->name }}
                    <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <img src="{{ Auth::guard('trainee')->user()->image }}" alt="user" class="rounded-circle">
                </a>
                            
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                    <a class="dropdown-item" href="{{route('trainee.trainee.profile')}}"><i class="mdi mdi-account-circle m-r-5 text-muted"></i> Profile</a>                    
                    <!-- <a class="dropdown-item" href="{{route('trainee.traineer.changepassword')}}"><i class="mdi mdi-lock-open-outline m-r-5 text-muted"></i> Change password</a> -->

                    <a class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('trainee.trainee.logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            <i class="mdi mdi-exit-to-app m-r-5 text-muted"></i>
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('trainee.trainee.logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </a>
                </div>               
            </li>
        </ul>

        <ul class="list-inline menu-left mb-0">
            <li class="list-inline-item">
                <button type="button" class="button-menu-mobile open-left waves-effect">
                    <i class="ion-navicon"></i>
                </button>
            </li>
        </ul>
        <div class="clearfix"></div>
    </nav>
</div>

       
