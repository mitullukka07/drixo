<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="ion-close"></i>
    </button>

    <div class="left-side-logo d-block d-lg-none">
        <div class="text-center">
            <a href="index.html" class="logo"><img src="{{asset('assets/images/logo-dark.png')}}" height="20" alt="logo"></a>
        </div>
    </div>

    <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
            <ul>
                    
                
                <li>
                    <a href="{{route('admin.dashboard')}}" class="waves-effect">
                        <i class="dripicons-meter"></i>
                        <span> Dashboard </span>
                    </a>
                </li>
                               
                <li>
                    <a href="{{route('admin.department.index')}}" class="waves-effect">
                        <i class="dripicons-user"></i>
                        <span> Department </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.trainee.index')}}" class="waves-effect">
                        <i class="dripicons-user"></i>
                        <span> Trainee </span>
                    </a>
                </li>


                <li>
                    <a href="{{route('admin.traineer.index')}}" class="waves-effect">
                        <i class="dripicons-user"></i>
                        <span> Trainer </span>
                    </a>
                </li>



                <li>
                    <a href="{{route('admin.task.index')}}" class="waves-effect">
                        <i class="fa fa-tasks"></i>
                        <span> Task </span>
                    </a>
                </li>

               

                <!-- <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="dripicons-briefcase"></i> <span> Elements </span> <span class="menu-arrow float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="ui-alerts.html">Alerts</a></li>
                                    <li><a href="ui-buttons.html">Buttons</a></li>
                                    <li><a href="ui-badge.html">Badge</a></li>
                                    <li><a href="ui-cards.html">Cards</a></li>
                                  
                                </ul>
                            </li> -->
            </ul>
        </div>
        <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
</div>