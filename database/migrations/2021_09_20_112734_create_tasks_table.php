<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('descrption');
            $table->bigInteger('traineer_id')->unsigned();
            $table->foreign('traineer_id')->references('id')->on('traineers')->onDelete('cascade')->onUpdate('cascade');
            $table->longText('trainee_id');
            $table->string('image');
            $table->integer('status');   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
