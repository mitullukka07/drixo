<?php       
          
        public function  store()           
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ route("admin.department.store") }}',
                        type: 'post',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "JSON",
                        success: function(result) {
                            swal({
                                title: "Inserted",
                                text: "Insert Succesfully!",
                                buttons: ["Cancel", "Submit"]
                            }).then(function(isConfirm) {
                                if (isConfirm) {
                            $('#myform')[0].reset();
                            window.location.href = "../department/index";
                        } else {
                            swal("Cancelled", "", "error")
                        }
                    });
                        },
                        error: function(result) {
                            $('#nameError').text(result.responseJSON.errors.name);
                            $('#imageError').text(result.responseJSON.errors.image);
                        }
                    });
               
        
   
?>