<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;


class Traineer extends Authenticatable
{
    use HasFactory,HasApiTokens;
    protected $guard = 'traineer';
    
    protected $fillable = [
        'name','email','password','mobile','image','address','department_id','github_id'
    ];

    public function department()
    {
        return $this->belongsTo(Department::class,'department_id');
    }

    public function getImageAttribute($value)
    {
        return $value ? asset('storage/UserProfilepicture' .'/'. $value) : NULL;
    }
}
