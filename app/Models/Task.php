<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'title','descreption','traineer_id','trainee_id','image','start_date','end_date','status'
    ];

    protected $casts = [
        'trainee_id' => 'array',
    ];

    public function traineer()
    {
        return $this->belongsTo(Traineer::class,'traineer_id');
    }

    // public function trainee()
    // {
    //     return $this->hasMany(Trainee::class,'trainee_id');
    // }


    // public function setTraineeidAttribute($value)
    // {
    //     $this->attributes['trainee_id'] = json_encode($value);
    // }

    // public function getTraineeidAttribute($value)
    // {
    //     return $this->attributes['trainee_id'] = json_decode($value);
    // }
  

}
 