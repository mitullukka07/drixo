<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','image'
    ];

    public function getImageAttribute($value)
    {
        return $value ? asset('storage/UserProfilepicture' . '/' . $value) : NULL;
    }

    public function getNameAttribute($value)
    {
        return ucFirst($value);
    }
}
