<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\DepartmentRepository;
use App\Interfaces\DepartmentRepositoryInterface;
use App\Interfaces\TaskRepositoryInterface;
use App\Interfaces\TraineeRepositoryInterface;
use App\Interfaces\TraineerRepositoryInterface;
use App\Repository\TraineerRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DepartmentRepositoryInterface::class,DepartmentRepository::class);
        $this->app->bind(TraineerRepositoryInterface::class,TraineerRepository::class);
        $this->app->bind(TraineeRepositoryInterface::class,TraineeRepository::class);
        $this->app->bind(TaskRepositoryInterface::class,TaskRepository::class);
    }
}
