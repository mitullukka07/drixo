<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        // if (! $request->expectsJson()) {
        //     if($request->routeIs('admin.*'))
        //     {
        //         return route('admin.login');
        //     }
        //     if($request->routeIs('traineers.*'))
        //     {
        //         return route('traineers.traineerlogin');
        //     }
        //     if($request->routeIs('trainee.*'))
        //     {
        //         return route('trainee.traineelogin');
        //     }
        // }
          
        if (strpos($request->getPathInfo(), '/admin')) {
            return route('admin.login');
        }
        if (strpos($request->getPathInfo(), '/trainer-login')) {
            return route('traineers.login');
        }
        if (strpos($request->getPathInfo(), '/trainee')) {
            return route('trainee.login');
        }
        // return route('login');
    }
}

