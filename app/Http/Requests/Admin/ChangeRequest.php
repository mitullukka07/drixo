<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ChangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
         
                'oldpassword'     => 'required',
                'newpassword'     => 'required'
                
            
        ];        
    }
    public function message()
        {
            return [
                'oldpassword'=>'Please enter old password',
                'newpassword'=>'Please enter new password'
            ];
        }
}
