<?php

namespace App\Http\Requests\Traineer;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:3',
            'email'=>'required|email|unique:traineers',
            'mobile'=>'required|numeric|digits_between:7,11|unique:traineers',
            'image'=>'required|mimes:jpeg,jpg,png',
            'address'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Please enter name',
            'email.required'=>'Please enter email',
            'mobile.required'=>'Please enter mobile',
            'image.required'=>'Please enter image',
            'address.required'=>'Please enter address'
        ];
    }
}
