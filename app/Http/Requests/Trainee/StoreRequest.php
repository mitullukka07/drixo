<?php

namespace App\Http\Requests\Trainee;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'   => 'required|min:3',
            'email'  => 'required|email|unique:trainees',
            'mobile' => 'required|numeric|unique:trainees|digits_between:7,11',
            'image'  => 'required|mimes:jpeg,jpg,png',
            'doj'    => 'required',
            'address'=> 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'   => 'Please enter name',
            'email.required'  => 'Please enter email',
            'mobile.required' => 'Please enter mobile',
            'image.required'  => 'Please select image',
            'image.mimes'     => 'Only jpeg,jpg,png allowed',
            'doj.required'    => 'Please enter doj',
            'address.required'=> 'Please enter address' 
        ];
    }
}
