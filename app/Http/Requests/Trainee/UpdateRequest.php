<?php

namespace App\Http\Requests\Trainee;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;
        $imageRule = is_null('if image field is null') ? 'required' : 'nullable';
        return [
            'name'   => 'required|min:3',
            'email'  => 'required|unique:trainees,email,'.$id,
            'mobile' => 'required|digits_between:7,11|unique:trainees,mobile,'.$id,
            'image'  => [$imageRule],
            'doj'    => 'required',
            'address'=> 'required'
        ];

    }
    public function messages()
    {
        return [
            'name.required'   => 'Please enter name',
            'email.required'  => 'Please enter email',
            'mobile.required' => 'Please enter mobile',
            'image.required'  => 'Please select image',
            'doj.required'    => 'Please enter doj',
            'address.required'=> 'Please enter address' 
        ];

    }
}



