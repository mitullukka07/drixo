<?php

namespace App\Http\Requests\Department;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\UpperCase;
class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $imageRule = is_null('if image field is null') ? 'required' : 'nullable';
        return [
            'name'=>['required','min:3',new Uppercase],
            'image'=>[$imageRule]
            
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Please enter name',
            
        ];
    }
}
