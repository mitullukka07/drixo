<?php

namespace App\Http\Requests\Department;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\UpperCase;
class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>['required','min:3',new Uppercase],
            'image'=>'required|mimes:jpeg,jpg,png'
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Please enter name',
            'image'=>'Please select image'
        ];
    }
}
