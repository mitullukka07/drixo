<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'descreption'=>'required',
            'traineer_id'=>'required',
            'trainee_id'=>'required',
            'start_date'=>'required',
            'image'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please enter title',
            'descreption.required' => 'Please enter descreption',
            'traineer_id.required' => 'Please select',
            'start_date.required'   => 'Please select',
            'image.required' => 'Please select image',
        ];
    }
}
