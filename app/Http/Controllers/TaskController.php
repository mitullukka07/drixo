<?php

namespace App\Http\Controllers;

use App\DataTables\TaskDataTable;
use App\Http\Requests\Task\StoreRequest;
use App\Http\Requests\Task\UpdateRequest;
use App\Models\Task;
use App\Models\Trainee;
use App\Models\Traineer;
use App\Repository\TaskRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class TaskController extends Controller
{
    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TaskDataTable $dataTable)
    {
        return $dataTable->render('admin.task.index');
    }
    public function index2(TaskDataTable $dataTable)
    {
        $traineer_id = Auth::user('traineer')->id;     
        return $dataTable->with('traineer_id',$traineer_id)->render('traineer.task.index');
    }
    public function index3(TaskDataTable $dataTable)
    {
        $task = Task::all();
        foreach ($task as $t) {
            foreach ($t->trainee_id as $id) {
                if ($id == Auth::user()->id) {
                    $task_id[] = $t->id;
                }
            }
        }
        return $dataTable->with('id',$task_id)->render('trainee.task.index');        
    }
    
    public function create()
    {
        $traineer = Traineer::all();
        $trainee = Trainee::all();
        return view('admin.task.create', compact('traineer', 'trainee'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return $this->taskRepository->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task, $id)
    {
       return $this->taskRepository->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task, $id)
    {
        return $this->taskRepository->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Task $task)
    {            
        return $this->taskRepository->update($request);
    }

   
    public function destroy(Task $task)
    {
        
    }

    public function changeStatus(Request $request)
    {
       $this->taskRepository->changestatus($request);
    }
}
