<?php

namespace App\Http\Controllers;

use App\DataTables\TraineersDataTable;
use App\Http\Requests\Admin\ChangeRequest;
use App\Http\Requests\Traineer\StoreRequest;
use App\Http\Requests\Traineer\UpdateRequest;
use Illuminate\Support\Facades\File;
use App\Models\Department;
use App\Models\Task;
use App\Models\Traineer;
use App\Repository\TraineerRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class TraineerController extends Controller
{

    public function __construct(TraineerRepository $traineerRepository)
    {
        $this->traineerRepository = $traineerRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TraineersDataTable $dataTable)
    {
        $traineer_id = Auth::user('traineer')->id;                   
        return $dataTable->with('traineer_id',$traineer_id)->render('traineer.traineer.index');                
    }
    public function index2(TraineersDataTable $dataTable)
    {        
        return $dataTable->render('admin.traineer.index');                
    }
    public function index3(TraineersDataTable $dataTable)
    {     
        $trainee_traineer_id = Auth::user('trainee')->id;        
        return $dataTable->with('trainee_traineer_id',$trainee_traineer_id)->render('trainee.traineer.index');                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department = Department::all();
        $select = [];
        foreach ($department as $dept) {
            $select[$dept->id] = $dept->name;
        }
        return view('admin.traineer.create',compact('select'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return  $this->traineerRepository->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Traineer  $traineer
     * @return \Illuminate\Http\Response
     */
    public function show(Traineer $traineer,$id)
    {
        return $this->traineerRepository->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Traineer  $traineer
     * @return \Illuminate\Http\Response
     */
    public function edit(Traineer $traineer,$id)
    {
       return $this->traineerRepository->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Traineer  $traineer
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Traineer $traineer)
    {
        return $this->traineerRepository->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Traineer  $traineer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Traineer $traineer)
    {
        //
    }

    public function profile(Traineer $traineer)
    {        
       return $this->traineerRepository->profile();
    }

    public function changepassword()
    {
        return view('traineer.changepassword');
    }

    public function updatepassword(ChangeRequest $request)
    {
        return $this->traineerRepository->updatepassword($request);  
    }

    public function updateprofile(Request $request)
    {
        return $this->traineerRepository->updateprofile($request);
    }
}
