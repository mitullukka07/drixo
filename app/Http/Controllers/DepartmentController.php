<?php

namespace App\Http\Controllers;

use App\Http\Requests\Department\StoreRequest;
use App\Models\Department;
use Illuminate\Http\Request;
use App\DataTables\DepartmentsDataTable;
use App\Http\Requests\Department\UpdateRequest;
use Illuminate\Support\Facades\File;
use App\Repository\DepartmentRepository;

class DepartmentController extends Controller
{

    private $departmentRepository;

    public function __construct(DepartmentRepository $departmentRepository)
    {
        $this->departmentRepository = $departmentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DepartmentsDataTable $dataTable)
    {
        return $dataTable->render('traineer.department.index');
    }
    public function index2(DepartmentsDataTable $dataTable)
    {
        return $dataTable->render('traineer.department.index');
    }
    public function index3(DepartmentsDataTable $dataTable)
    {
        return $dataTable->render('trainee.department.index');  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {        
        return $this->departmentRepository->store($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->departmentRepository->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department, $id)
    {
       return $this->departmentRepository->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        return $this->departmentRepository->updatedep($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
