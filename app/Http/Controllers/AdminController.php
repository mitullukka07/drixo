<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\ChangeRequest;

use App\Models\Admin;
use App\Repository\AdminRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    public function __construct(AdminRepository $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }
    public function profile(Admin $admin)
    {        
       return $this->adminRepository->profile();
    }
    public function changepassword()
    {
        return view('admin.changepassword');
    }

    public function updatepassword(ChangeRequest $request)
    {
       return $this->adminRepository->updatepassword($request);
    } 

    public function updateprofile(Request $request)
    {
        return $this->adminRepository->updateprofile($request);
    }
        
    
}
