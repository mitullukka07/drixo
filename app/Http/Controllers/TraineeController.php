<?php

namespace App\Http\Controllers;

use App\DataTables\TraineesDataTable;
use App\Http\Requests\Trainee\StoreRequest;
use App\Http\Requests\Trainee\UpdateRequest;
use App\Models\Department;
use App\Models\Task;
use App\Models\Trainee;
use App\Repository\TraineeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;


class TraineeController extends Controller
{

    public function __construct(TraineeRepository $traineeRepository)
    {
         $this->traineeRepository = $traineeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TraineesDataTable $dataTable)
    {
        return $dataTable->render('admin.trainee.index');        
    }
    public function index2(TraineesDataTable $dataTable)
    {     
        $task_trainee_id = Task::where('traineer_id',Auth::user()->id)->select('trainee_id')->get();     
        return $dataTable->with('task_trainee_id',$task_trainee_id)->render('traineer.trainee.index');                
    }
    public function index3(TraineesDataTable $dataTable)
    {    
        $trainee_id = Auth::user('traineer')->id;  
        return $dataTable->with('trainee_id',$trainee_id)->render('trainee.trainee.index');                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->traineeRepository->create();
    }

    /**
     * Store a newly created resource in storage.
     *  
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return $this->traineeRepository->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Trainee  $trainee
     * @return \Illuminate\Http\Response
     */
    public function show(Trainee $trainee,$id)
    {
       return $this->traineeRepository->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Trainee  $trainee
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainee $trainee,$id)
    {
        return $this->traineeRepository->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Trainee  $trainee
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Trainee $trainee)
    {
       return $this->traineeRepository->update($request);
    }

   
    public function destroy(Trainee $trainee)
    {
        
    }

    public function profile()
    {
        return $this->traineeRepository->profile();
    }

    public function updateprofile(Request $request)
    {
        return $this->traineeRepository->updateprofile($request);
    }
}


