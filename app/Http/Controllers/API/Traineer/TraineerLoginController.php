<?php

namespace App\Http\Controllers\API\Traineer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TraineerLoginController extends Controller
{
    public $successStatus = 200;
    public function TraineerLogin(Request $request)
    {
        $request->validate([
              'email'   => 'required',
              'password'=>'required'  
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::guard('traineer')->attempt($credentials)) {
            $traineer = Auth::guard('traineer')->user();
            $success['token'] =  $traineer->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function traineerLogout(Request $request)
    {
        $token = auth()->user()->token();
        $token->revoke();
        return response(['message' => 'You have been successfully logged out.'], 200);
    }
}
?>
