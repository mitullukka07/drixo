<?php

namespace App\Http\Controllers\API\Traineer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Traineer\StoreRequest;
use App\Models\Traineer;
use App\Repository\TraineerRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;

class TraineerController extends Controller
{
    public $successStatus = 200;
    public function __construct(TraineerRepository $traineerRepository)
    {
        $this->traineerRepository = $traineerRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $traineer = Traineer::all();
        return response()->json(['traineer'=>$traineer]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        return  $this->traineerRepository->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $traineer = Traineer::find($id);
        return response()->json(['traineer'=>$traineer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // $traineer = Traineer::find($request->id);
        // $update = [
        //     'name'          =>    empty($request->name) ? $traineer->name : $request->name,
        //     'email'         =>    empty($request->email) ? $traineer->email : $request->email,
        //     'mobile'        =>    empty($request->mobile) ? $traineer->mobile : $request->mobile,
        //     'address'       =>    empty($request->address) ? $traineer->address : $request->address,
        //     'department_id' =>    empty($request->department_id) ? $traineer->department_id : $request->department_id,
            
        // ];
        // Traineer::where('id',$request->id)->update($update);

        // if($request->hasFile('image'))
        // {
        //     $destination = 'traineer/'.$traineer->image;
        //     if(File::exists($destination))
        //     {
        //         File::delete($destination);
        //     }
        //     $file = $request->file('image');
        //     $name = $file->getClientOriginalName();
        //     $file->move('traineer/',$name);
        //     $traineer->image = $name;
        // }
        
        // if($traineer->update())
        // {
        //     return response()->json(['success'=>'Success','traineer'=>$traineer]);    
        // }else{
        //     return response()->json(['error'=>'error']);
        // }  
        return  $this->traineerRepository->update($request);      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profile()
    {        
        return $this->traineerRepository->profile();
    }
}


