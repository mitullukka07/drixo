<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Department\StoreRequest;
use Illuminate\Http\Request;
use App\Repository\DepartmentRepository;
use Illuminate\Support\Facades\File;
use App\Models\Department;


class DepartmentController extends Controller
{
    public $successStatus = 200;
    public function __construct(DepartmentRepository $departmentRepository)
    {
        $this->departmentRepository = $departmentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = Department::all();
        return response()->json(['department' => $department]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        // $image = uploadFile($request->image, 'UserProfilepicture');
        // $companie = new Department;
        // $companie->name = $request->name;
        // $companie->image = $image;
        // if($companie->save())
        // {
        //     return response()->json(['success' => 'Success'], $this-> successStatus);
        // }else{
        //     return response()->json(['error'=>'Unauthorised'], 401); 
        // }

        return $this->departmentRepository->store($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::find($id);
        return response()->json(['department'=>$department]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $department = Department::find($request->id);
        $department->name = $request->name;
        if ($request->hasFile('image')) {
            $destination = 'storage/UserProfilepicture/' . $department->image;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $request->file('image');
            $name = $file->getClientOriginalName();
            $name = $file->hashName();
            $file->move('storage/UserProfilepicture/', $name);
            $department->image = $name;
        }
        if($department->save())
        {
            return response()->json(['update' => 'Success','department'=>$department]);
        }else{
            return response()->json(['error'=>'Unauthorised'], 401); 
        }
        // return response()->json('1');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
