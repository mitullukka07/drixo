<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Models\Trainee;
use App\Models\Traineer;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class TraineeAuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = "/trainee/traineedashbord";

    public function __construct()
    {
        $this->middleware('guest:trainee')->except('logout');
    }
    
    public function showLoginForm()
    {
        return view('admin.traineelogin');
    }
    
    protected function attemptLogin(Request $request)
    {        
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::guard('trainee')->attempt($credentials)) {
            $businessData=Trainee::where('email',$request->email);
                return redirect()->route('trainee.traineedashbord');
            
        }   
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();        
        return redirect()->route('trainee.login');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('trainee');
    }

    public function redirectToLinkedin()
    {
        return Socialite::driver('linkedin')->redirect();        
    }

    public function handleLinkedinCallback()
    {
        $user =  Socialite::driver('linkedin')->user();
        dd($user);        
        return view('admin.dashboard',compact('user'));
    }
}
