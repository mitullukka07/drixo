<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Models\Trainee;
use App\Models\Traineer;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class TraineerAuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = "/trainer-login/traineerdashboard";

    public function __construct()
    {
        $this->middleware('guest:traineer')->except('logout');
    }

    public function showLoginForm()
    {
        return view('admin.traineerlogin');
    }

    protected function attemptLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::guard('traineer')->attempt($credentials)) {
            $businessData = Traineer::where('email', $request->email);
            return redirect()->route('traineers.traineerdashboard');
        }   
    }

    public function logout(Request $request)
    {
        
        $this->guard()->logout();
        return  redirect()->route('traineers.login');        
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('traineer');
    }

    public function redirectToGitHub()
    {    
        return Socialite::driver('github')->redirect();
    }

    public function handleGitHubCallback()
    {
        $userSocial =  Socialite::driver('github')->stateless()->user();
        $user = Traineer::where('github_id', $userSocial->id)->first();
        if(!$user){
            $newuser = Traineer::create([
                'name' => $userSocial->name,
                'email' => $userSocial->email,
                'password' => encrypt('123456'),
                'github_id' => $userSocial->id
            ]);
        }
        Auth::guard('traineer')->login($user);
        return view('admin.traineerdashboard', compact('user'));         
    }
}


 