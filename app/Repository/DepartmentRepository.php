<?php

namespace App\Repository;

use App\Interfaces\DepartmentRepositoryInterface;
use Illuminate\Support\Facades\File;
use App\Models\Department;

class DepartmentRepository implements DepartmentRepositoryInterface
{
    public function store(array $array)
    {              
        $image = uploadFile($array['image'], 'UserProfilepicture');
        $companie = new Department;
        $companie->name = $array['name'];
        $companie->image = $image;
        $companie->save();
        return response()->json('1');
    }

    public function show($id)
    {
        $department = Department::find($id);
        return response()->json($department);
    }

    public function edit($id)
    {
        $department = Department::find($id);
        return response()->json($department);
    }

    public function updatedep($array)
    {        
        $department = Department::find($array->id);
        $department->name = $array->name;
        if ($array->hasFile('image')) {
            $destination = 'storage/UserProfilepicture/' . $department->image;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $array->file('image');
            $name = $file->getClientOriginalName();
            $name = $file->hashName();
            $file->move('storage/UserProfilepicture/', $name);
            $department->image = $name;
        }
        $department->save();
        return response()->json('1');
    }

    public function destroy()
    {

    }
}

?>