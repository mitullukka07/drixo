<?php

namespace App\Repository;

use App\Interfaces\TraineerRepositoryInterface;
use Illuminate\Support\Facades\File;
use App\Models\Traineer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class TraineerRepository implements TraineerRepositoryInterface
{
    public function store($array)
    {
        
        $image  = uploadFile($array->image,'UserProfilepicture');
        $traineer = new Traineer();
        $password = rand(1000,9999);
        $traineer->name       =    $array->name;
        $traineer->email      =    $array->email;
        $traineer->password   =    bcrypt($password);
        $traineer->mobile     =    $array->mobile;
        $traineer->image      =    $image;
        $traineer->address    =    $array->address;
        $traineer->department_id    =    $array->department_id;
        if($traineer->save())
        {
            $data = ['title' => 'password','password'=>$password];
            $user['to'] = $array->email;
            Mail::send('admin.traineer.mail', $data, function ($message) use ($user) {
                $message->to($user['to']);
                $message->subject('Your login password');
            });
        }
        return response()->json('1');
    }

    public function show($id)
    {
        $traineer = Traineer::find($id);
        $department = $traineer->department->name;
        return response()->json(['traineer'=>$traineer,'department'=>$department]);
    }

    public function edit($id)
    {
        $traineer = Traineer::find($id);
        $department=$traineer->department->id;
        return response()->json(['traineer'=>$traineer,'department'=>$department]);
    }

    public function update($array)
    {
        // $traineer = Traineer::find($array->id);
        // $traineer->name       =    $array->name;
        // $traineer->email      =    $array->email;
        // $traineer->mobile     =    $array->mobile;
        // $traineer->address    =    $array->address;
        // $traineer->department_id    =    $array->department_id;

        // if($array->hasFile('image'))
        // {
        //     $destination = 'traineer/'.$traineer->image;
        //     if(File::exists($destination))
        //     {
        //         File::delete($destination);
        //     }
        //     $file = $array->file('image');
        //     $name = $file->getClientOriginalName();
        //     $file->move('traineer/',$name);
        //     $traineer->image = $name;
        // }
        
        // $traineer->update();        
        // return response()->json($traineer);
        $traineer = Traineer::find($array->id);
        $update = [
            'name'          =>    empty($array->name) ? $traineer->name : $array->name,
            'email'         =>    empty($array->email) ? $traineer->email : $array->email,
            'mobile'        =>    empty($array->mobile) ? $traineer->mobile : $array->mobile,
            'address'       =>    empty($array->address) ? $traineer->address : $array->address,
            'department_id' =>    empty($array->department_id) ? $traineer->department_id : $array->department_id, 

        ];    
        if($array->hasFile('image'))
        {
            $destination = 'storage/UserProfilepicture/'.$traineer->image;
            if(File::exists($destination))
            {
                File::delete($destination);
            }
            $file = $array->file('image');
            $name = $file->getClientOriginalName();
            $file->move('storage/UserProfilepicture/',$name);
            // $traineer->image = $name;
            $update['image'] = $name;
        }
        
        Traineer::where('id',$array->id)->update($update);
        
        if($traineer->update())
        {
            return response()->json(['success'=>'Success','traineer'=>$traineer]);    
        }else{
            return response()->json(['error'=>'error']);
        }        
    }

    public function destroy()
    {
        
    }

    public function profile()
    {
        $traineer = Auth::guard('traineer')->user();     
        return view('traineer.profile',compact('traineer'));
    }

    public function updatepassword($array)
    {
        $hashedPassword = Auth::guard('traineer')->user()->password;
        if (Hash::check($array->oldpassword, $hashedPassword)) {
            $update = bcrypt($array->newpassword);
            $users = Auth::guard('traineer')->id();
            $id = Traineer::where('id', $users)->update(['password' => $update]);
            if ($id) {
                session()->flash('update', 'password updated successfully');
            }
            return redirect()->back();
        } else {
            session()->flash('error', 'old password doesnt matched');
            return redirect()->back();
        }   
    }

    public function updateprofile($array)
    {
        $trainee = Auth::guard('traineer')->user();
        $trainee->name = $array->name;
        $trainee->address = $array->address;
        if($trainee->save())
        {
            session()->flash('update','Profile updated successfully');
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }
}
?>