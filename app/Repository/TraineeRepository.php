<?php


namespace App\Repository;

use App\Interfaces\TraineeRepositoryInterface;
use App\Mail\TraineeMail;
use App\Models\Department;
use Illuminate\Support\Facades\File;
use App\Models\Trainee;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class TraineeRepository implements TraineeRepositoryInterface
{
    public function create()
    {
        $department = Department::all(); 
        $select = [];      
        foreach($department as $dep){
            $select[$dep->id] = $dep->name;
        }        
        return view('admin.trainee.create',compact('select'));
    }
    public function store($array)
    {
        $image = uploadFile($array->image,'UserProfilepicture');
        $trainee = new Trainee;
        $password = rand(1000,9999);
        $trainee->name      =   $array->name;
        $trainee->email     =   $array->email;
        $trainee->password  =   bcrypt($password);
        $trainee->mobile    =   $array->mobile;
        $trainee->image     =   $image;
        $trainee->doj       =   $array->doj;
        $trainee->address   =   $array->address;
        $trainee->department_id =  $array->department_id;
        
        if($trainee->save())
        {
            $data = ['title'=>'title','password'=>$password];
            // Mail::to($trainee)->send(new TraineeMail());
             //return $trainee;
            $user['to'] = $array->email;
            Mail::send('admin.trainee.mail',$data,function($message) use ($user){
                $message->to($user['to']);
                $message->subject('Your login password');
            });
        }
        return response()->json($trainee); 
    }

    public function show($id)
    {
        $trainee = Trainee::find($id);
        $department = $trainee->department->name;
        return response()->json(['trainee'=>$trainee,'department'=>$department]);
    }

    public function edit($id)
    {
        $trainee = Trainee::find($id);
        $department = $trainee->department->id;
        return response()->json(['trainee'=>$trainee,'department'=>$department]);
    }

    public function update($array)
    {
        // $trainee = Trainee::find($array->id);
        // $trainee->name      =   $array->name;
        // $trainee->email     =   $array->email;
        // $trainee->mobile    =   $array->mobile;
        // $trainee->doj       =   $array->doj;
        // $trainee->address   =   $array->address;
        // $trainee->department_id =  $array->department_id;

        // if ($array->hasFile('image')) {
        //     $destination = 'trainee/' . $trainee->image;
        //     if (File::exists($destination)) {
        //         File::delete($destination);
        //     }
        //     $file = $array->file('image');
        //     $name = $file->getClientOriginalName();
        //     $file->move('trainee/', $name);
        //     $trainee->image = $name;
        // }
        // $trainee->update();
        // return response()->json('1');

            $trainee = Trainee::find($array->id);
            $update = [
            'name'      =>   empty($array->name) ? $trainee->name : $array->name ,
            'email'     =>   empty($array->email) ? $trainee->email : $array->email,
            'mobile'    =>   empty($array->mobile) ? $trainee->mobile : $array->mobile,
            'doj'       =>   empty($array->doj) ? $trainee->doj : $array->doj,
            'address'   =>   empty($array->address) ? $trainee->address : $array->address,
            'department_id' =>  empty($array->department_id) ? $trainee->department_id : $array->department_id ,
            ];
            
            if ($array->hasFile('image')) {
                $destination = 'trainee/' . $trainee->image;
                if (File::exists($destination)) {
                    File::delete($destination);
                }
                $file = $array->file('image');
                $name = $file->getClientOriginalName();
                $file->move('trainee/', $name);
                $trainee->image = $name;
            }
            Trainee::where('id',$array->id)->update($update);
            if($trainee->update())
            {
                return response()->json(['success'=>'Success','trainee'=>$trainee]);    
            }else{
                return response()->json(['error'=>'error']);
            } 

    }

    public function destroy()
    {

    }

    public function profile()
    {
        $trainee = Auth::guard('trainee')->user();
        return view('trainee.profile',compact('trainee'));
    }

    public function updateprofile($array)
    {
        $trainee = Auth::guard('trainee')->user();
        $trainee->name = $array->name;
        if($trainee->save())
        {
            session()->flash('update','Profile updated successfully');
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }
}
?>


