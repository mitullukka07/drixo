<?php

namespace App\Repository;

use App\Interfaces\TaskRepositoryInterface;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use App\Models\Task;
use App\Models\Trainee;

class TaskRepository implements TaskRepositoryInterface
{
    
    public function store($array)
    {        
        $image = array();
        if ($files = $array->file('image')) {
            foreach ($files as $file) {
                $name = time() . '.' . $file->getClientOriginalName();
                $file->move('uploads/', $name);
                $image[] = $name;
            }
        }
        $task = new Task;
        $task->title       = $array->title;
        $task->descrption  = $array->descrption;
        $task->traineer_id = $array->traineer_id;
        $task->trainee_id  = $array->trainee_id;
        $task->image       = implode(',', $image);
        $task->start_date  = $array->start_date;
        $task->end_date    = $array->end_date;
        $task->status      = $array->status;
        $task->save();
        return response()->json('1');
    }

    public function show($id)
    {
        $task = Task::find($id);
        $traineer = $task->traineer->name;

        $trainee = Trainee::whereIn('id', $task->trainee_id)->get();
        $select = "";
        foreach ($trainee as $traine) {
            $select .= " " . $traine->name;
        }
        return response()->json(['task' => $task, 'traineer' => $traineer, 'trainee' => $select]);
    }

    public function edit($id)
    {
        $task = Task::find($id);
        $traineer = $task->traineer->id;
        $trainee = Trainee::whereIn('id', $task->trainee_id)->get();
        return response()->json(['task' => $task, 'traineer' => $traineer, 'trainee' => $trainee]);
    }

    public function update($array)
    {
        // $task = Task::find($array->id);
        // $task->title        =   $array->title;
        // $task->descrption   =   $array->descrption;
        // $task->traineer_id  =   $array->traineer_id;
        // $task->trainee_id   =   $array->trainee_id;
        // $task->start_date   =   $array->start_date;
        // $task->end_date     =   $array->end_date;
        // $task->status       =   $array->status;
        // $image = array();
        // if ($array->hasFile('image')) {
        //     $destination = 'uploads/' . $task->image;
        //     if (File::exists($destination)) {
        //         File::delete($destination);
        //     }
        //     if ($files = $array->file('image')) {
        //         foreach ($files as $file) {
        //             $name = time() . '.' . $file->getClientOriginalName();
        //             $file->move('uploads/', $name);
        //             $task->image  =  implode(',', $image);
        //             $image[] = $name;
        //         }
        //     }
        // }
        // $task->update();
        // return response()->json($task);

        $task = Task::find($array->id);
        $update  = [
        'title'        =>   empty($array->title) ? $task->title : $array->title,
        'descrption'   =>   empty($array->descrption) ? $task->descrption : $array->descrption,
        'traineer_id'  =>   empty($array->traineer_id) ? $task->traineer_id : $array->traineer_id,
        'trainee_id'   =>   empty($array->trainee_id) ? $task->trainee_id : $array->trainee_id,
        'start_date'   =>   empty($array->start_date) ? $task->start_date : $array->start_date,
        'end_date'     =>   empty($array->end_date) ? $task->end_date : $array->end_date,
        'status'       =>   empty($array->status) ? $task->status : $array->status,
        ];
        Task::where('id',$array->id)->update($update);
        $image = array();
        if ($array->hasFile('image')) {
            $destination = 'uploads/' . $task->image;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            if ($files = $array->file('image')) {
                foreach ($files as $file) {
                    $name = time() . '.' . $file->getClientOriginalName();
                    $file->move('uploads/', $name);
                    $task->image  =  implode(',', $image);
                    $image[] = $name;
                }
            }
        }
        if($task->update())
        {
            return response()->json(['success'=>'Success','trainee'=>$task]);    
        }else{
            return response()->json(['error'=>'error']);
        } 
    }

    public function destroy()
    {

    }

    public function changestatus($array)
    {
        $task = Task::find($array->id);
        $trainee = Trainee::whereIn('id', $task->trainee_id)->get();
        $traineer = $task->traineer->name;
        $task->status = $array->status;
        
        if($array->status ==  0)
        { 
            foreach ($trainee as $mail)
            {
                $task->status = 1;                          
                $data = ['title'=>$task];
                $user['to'] =$mail->email;   
                Mail::send('admin.task.mail',$data,function($message) use ($user){
                    $message->to($user['to']);
                    $message->subject('Hello');
                });
            }
        }else{
            $task->status = 0;
        }
        $task->save();        
        return response()->json('1');
    }
}


?>