<?php
    namespace App\Repository;

use App\Interfaces\AdminRepositoryInterface;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminRepository implements AdminRepositoryInterface
{
    public function profile()
    {
        $admin = Auth::guard('admin')->user();     
        return view('admin.profile',compact('admin'));
    }

    public function updateprofile($array)
    {
        $admin = Auth::guard('admin')->user();
        $admin->name = $array->name;
        if($admin->save())
        {
            session()->flash('update','Profile updated successfully');
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function updatepassword($array)
    {
        $hashedPassword = Auth::guard('admin')->user()->password;
        if (Hash::check($array->oldpassword, $hashedPassword)) {
            $update = bcrypt($array->newpassword);
            $users = Auth::guard('admin')->id();
            $id = Admin::where('id', $users)->update(['password' => $update]);
            if ($id) {
                session()->flash('update', 'password updated successfully');
            }
            return redirect()->back();
        }
        else{
            session()->flash('error','old password doesnt matched');
            return redirect()->back();
        }
    }  
}
?>
