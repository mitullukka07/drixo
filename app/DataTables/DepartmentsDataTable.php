<?php

namespace App\DataTables;

use App\Models\Department;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
 use Yajra\DataTables\Services\DataTable;

class DepartmentsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($data) {
                $btn = "";
                $btn = '<a href="' . route('admin.department.show', $data->id) . '" data-id="' . $data->id . '"  data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#myModalshow" class="edit btn btn-warning btn-sm btnshow"><i class="fa fa-eye"></i></a>&nbsp';
                $btn .= '<a href="' . route('admin.department.edit', $data->id) . '" data-id="' . $data->id . '" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" class="edit btn btn-info btn-sm btnedit"><i class="fa fa-pencil"></i></a>';

                return $btn;
            })
            ->editColumn('image', function ($data) {
                if ($data->image) {
                    return '<img src="' . $data->image . '"  style="width:60px; height:60px;   object-fit: cover; border-radius:0px;"/>';
                } else {
                    return '<img src="' . asset('images/no-image.jpg') . '" width=50px height=50px/>';
                }
            })
            ->rawColumns(['action', 'image'])
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Department $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Department $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('departments-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),  
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('No')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('id')->hidden(true),
            Column::make('name'),
            Column::make('image')->orderable(false),
            Column::computed('action')
            ->exportable(false)
            ->printable(false)
            ->width(60)
            ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Departments_' . date('YmdHis');
    }
}

