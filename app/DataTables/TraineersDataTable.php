<?php

namespace App\DataTables;

use App\Models\Trainee;
use App\Models\Traineer;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TraineersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($data) {
                $btn = "";
                if (Auth::guard('admin')->check()) {
                    $btn = '<a href="' . route('admin.traineer.show', $data->id) . '" data-id="' . $data->id . '" data-backdrop="static" data-keyboard="false"  data-toggle="modal"  data-target="#myModalshow" class="edit btn btn-warning btn-sm btnshow"><i class="fa fa-eye"></i></a>&nbsp';
                    $btn .= '<a href="' . route('admin.traineer.edit', $data->id) . '" data-id="' . $data->id . '" data-backdrop="static" data-keyboard="false"  data-toggle="modal"  data-target="#myModal" class="edit btn btn-info btn-sm editbtn"><i class="fa fa-pencil"></i></a>';
                } else {
                    $btn = '<a href="' . route('admin.traineer.show', $data->id) . '" data-id="' . $data->id . '"  style="display:none" data-backdrop="static" data-keyboard="false"  data-toggle="modal"  data-target="#myModalshow" class="edit btn btn-warning btn-sm btnshow"><i class="fa fa-eye"></i></a>&nbsp';
                    $btn .= '<a href="' . route('admin.traineer.edit', $data->id) . '" data-id="' . $data->id . '" style="display:none"  data-backdrop="static" data-keyboard="false"  data-toggle="modal"  data-target="#myModal" class="edit btn btn-info btn-sm editbtn"><i class="fa fa-pencil"></i></a>';
                }
                return $btn;
            })
            ->editColumn('mobile', function ($data) {
                if ($data->mobile == NULL) {
                    return '-';
                } else {
                    return $data->mobile;
                }
            })
            ->editColumn('image', function ($data) {
                // $url = asset('traineer/'.$data->image);
                if ($data->image == NULL) {
                    return '-';
                } else {
                    return '<img src="' . $data->image . '" width="50px" height="50px">';
                }
            })
            ->editColumn('address', function ($data) {
                if ($data->address == NULL) {
                    return '-';
                } else {
                    return $data->address;
                }
            })
            ->editColumn('department_id', function ($data) {
                if ($data->department_id == NULL) {
                    return '-';
                } else {
                    return $data->department->name;
                }
            })
            ->rawColumns(['action','mobile','address','department_id','image'])
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Traineer $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Traineer $model)
    {
        if ($this->traineer_id) {
            return $model->where('id', $this->traineer_id)->newQuery();
        } elseif ($this->trainee_traineer_id) {
            return $model->where('id', $this->trainee_traineer_id)->newQuery();
        } else {
            return $model->newQuery();
        }     
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('traineers-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bxfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        if ($this->traineer_id) {
            return [
                Column::make('no')->data('DT_RowIndex')->searchable(false)->orderable(false),
                Column::make('id')->hidden(true),
                Column::make('name'),
                Column::make('email'),
                Column::make('mobile'),
                Column::make('image'),
                Column::make('address'),
                Column::make('department_id')->title('Department'),

            ];
        }elseif($this->trainee_traineer_id){
            return [
                Column::make('no')->data('DT_RowIndex')->searchable(false)->orderable(false),
                Column::make('id')->hidden(true),
                Column::make('name'),
                Column::make('email'),
                Column::make('mobile'),
                Column::make('image'),
                Column::make('address'),
                Column::make('department_id')->title('Department'),
                
            ];
        } else {
            return [
                Column::make('no')->data('DT_RowIndex')->searchable(false)->orderable(false),
                Column::make('id')->hidden(true),
                Column::make('name'),
                Column::make('email'),
                Column::make('mobile'),
                Column::make('image'),
                Column::make('address'),
                Column::make('department_id')->title('Department'),
                Column::computed('action')
                    ->exportable(false)
                    ->printable(false)
                    ->width(60)
                    ->addClass('text-center'),
            ];
        }
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Traineers_' . date('YmdHis');
    }
}


