<?php

namespace App\DataTables;

use App\Models\Task;
use App\Models\Trainee;
use App\Models\Traineer;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TaskDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($data){
                $btn = "";
                if(Auth::guard('admin')->check())
                {
                    $btn = '<a href="'.route('admin.task.show',$data->id).'" data-id="'. $data->id .'"  data-backdrop="static" data-keyboard="false"  data-toggle="modal"  data-target="#myModalshow"  class="edit btn btn-warning btn-sm btnshow"><i class="fa fa-eye"></i> </a>&nbsp';
                    $btn .= '<a href="'.route('admin.task.edit',$data->id).'" data-id="'.$data->id.'" data-backdrop="static" data-keyboard="false"  data-toggle="modal"  data-target="#myModal" class="edit btn btn-info btn-sm editbtn"><i class="fa fa-pencil"></i> </a>';
                }else{
                    $btn = '<a href="'.route('admin.task.show',$data->id).'" style="display:none"  class="edit btn btn-warning btn-sm btnshow"><i class="fa fa-eye"></i> </a>&nbsp';
                    $btn .= '<a href="'.route('admin.task.edit',$data->id).'" data-id="'.$data->id.'" style="display:none"  data-backdrop="static" data-keyboard="false"  data-toggle="modal"  data-target="#myModal" class="edit btn btn-info btn-sm editbtn"><i class="fa fa-pencil"></i> </a>';
                }    
                return $btn;
            })
            ->editColumn('traineer_id',function($data){
                return $data->traineer->name;
            })
            ->editColumn('trainee_id', function ($data) {
                $trainee = Trainee::whereIn('id', $data->trainee_id)->get();
                $select = "";
                foreach ($trainee as $traine) {
                    $select .= '<span class="badge badge-danger">' . $traine->name . '</span>&nbsp;';
                }
                return $select;
            })
            ->editColumn('image', function ($data) {
                $url = $data->image;
                $multiimage = "";
                foreach (explode(',', $url) as $img) {
                    $multiimage .= '<img src="' . asset('uploads/' . $img) . '"  width="30px" height="30px">&nbsp';
                }
                return $multiimage;
            })
            ->editColumn('status', function ($data) {
                if ($data->status == 1) {
                    $statusbadge = '<span class="badge badge-success status" ><i class="fa fa-check"></i></span>';
                } else {
                    $statusbadge = '<span class="badge badge-danger status" data-id="' . $data->id . '" data-status="0">Deactive</span>';
                }
                return $statusbadge;
            })
            ->rawColumns(['action','status','traineer_id','trainee_id','image']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Task $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Task $model)
    {
        if ($this->trainee_id) {
            return $model->where('trainee_id', $this->trainee_id)->newQuery();
        } elseif ($this->traineer_id) {            
            return $model->where('traineer_id', $this->traineer_id)->newQuery();
        } elseif ($this->id) {
            return $model->whereIn('id', $this->id)->newQuery();
        } else {
            return $model->newQuery();
        } 
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('task-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        if ($this->id) {
            return [
                Column::make('id'),
                Column::make('title'),
                Column::make('descrption'),
                Column::make('traineer_id')->title('Assigning'),
                Column::make('trainee_id')->title('To Assign')->hidden(),
                Column::make('image'),
                Column::make('start_date'),
                Column::make('end_date'),
                Column::make('status')->hidden()
                    ->exportable(false)
                    ->printable(false)
                    ->width(60)
                    ->addClass('text-center'),
            ];
        } else {
            return [
                Column::make('id'),
                Column::make('title'),
                Column::make('descrption')->title('Description'),
                Column::make('traineer_id')->title('Assigning')->hidden(),
                Column::make('trainee_id')->title('To Assign'),
                Column::make('image'),
                Column::make('start_date'),
                Column::make('end_date'),
                Column::make('status'),
                Column::computed('action')
                    ->exportable(false)
                    ->printable(false)
                    ->width(60)
                    ->addClass('text-center'),
            ];
        }
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Task_' . date('YmdHis');
    }
}

