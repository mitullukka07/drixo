<?php

namespace App\DataTables;

use App\Models\Task;
use App\Models\Trainee;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TraineesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($data){
                $btn = "";
                if(Auth::guard('admin')->check())
                {
                    $btn = '<a href="'.route('admin.trainee.show',$data->id).'" data-id="'.$data->id.'"  data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#myModalshow" class="edit btn btn-warning btn-sm btnshow"><i class="fa fa-eye"></i></a>&nbsp';
                    $btn .= '<a href="'.route('admin.trainee.edit',$data->id).'" data-id="'.$data->id.'" data-backdrop="static" data-keyboard="false" data-toggle="modal"  data-target="#myModal"  class="edit btn btn-info btn-sm editbtn"><i class="fa fa-pencil"></i></a>';
                }else{
                    $btn = '<a href="'.route('admin.trainee.show',$data->id).'" data-id="'.$data->id.'"  data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#myModalshow" style="display:none" class="edit btn btn-warning btn-sm btnshow"><i class="fa fa-eye"></i></a>&nbsp';
                    $btn .= '<a href="'.route('admin.trainee.edit',$data->id).'" data-id="'.$data->id.'" data-backdrop="static" data-keyboard="false" data-toggle="modal"  data-target="#myModal"  style="display:none" class="edit btn btn-info btn-sm editbtn"><i class="fa fa-pencil"></i></a>';
                }    
                return $btn;
            })
            ->editColumn('image', function ($data) {
                return  '<img src="' . $data->image . '" width="50px" height="50px">';
            })
            ->editColumn('department_id', function ($data) {
                return $data->department->name;
            })

            ->rawColumns(['action', 'image'])
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Trainee $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Trainee $model)
    {
        if ($this->trainee_id) {
            return $model->where('id', $this->trainee_id)->newQuery();
        } elseif ($this->task_trainee_id) {
            foreach ($this->task_trainee_id as $value) {
                return $model->whereIn('id', $value->trainee_id)->newQuery();
            }
        } else {
            return $model->newQuery();
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('trainees-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {   
        
        if($this->trainee_id)
        {
        return [
            Column::make('No')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('id')->hidden(true),
            Column::make('name'),
            Column::make('email'),
            Column::make('mobile'),
            Column::make('image'),
            Column::make('doj'),
            Column::make('address'),
            Column::make('department_id')->title('Department'),
           
        ];
        } elseif ($this->task_trainee_id) {
            return [
                Column::make('No')->data('DT_RowIndex')->searchable(false)->orderable(false),
                Column::make('id')->hidden(true),
                Column::make('name'),
                Column::make('email'),
                Column::make('mobile'),
                Column::make('image'),
                Column::make('doj'),
                Column::make('address'),
                Column::make('department_id')->title('Department'),
            ];
        } else {
            return [
                Column::make('No')->data('DT_RowIndex')->searchable(false)->orderable(false),
                Column::make('id')->hidden(true),
                Column::make('name'),
                Column::make('email'),
                Column::make('mobile'),
                Column::make('image'),
                Column::make('doj'),
                Column::make('address'),
                Column::make('department_id')->title('Department'),
                Column::computed('action')
                      ->exportable(false)
                      ->printable(false)
                      ->width(60)
                      ->addClass('text-center'),
            ];
        }
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Trainees_' . date('YmdHis');
    }
}
