<?php


namespace App\Interfaces;

interface TaskRepositoryInterface
{
    public function store($array);
    public function show($id);
    public function edit($id);
    public function update($array);
    public function destroy();
    public function changestatus($array);
}  

?>