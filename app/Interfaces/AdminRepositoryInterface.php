<?php
    namespace App\Interfaces;

    interface AdminRepositoryInterface
    {
        public function profile();
        public function updatepassword($array);
        public function updateprofile($array);
    }
?>