<?php

namespace App\Interfaces;

interface TraineeRepositoryInterface
{
   public function create();
   public function store($array);
   public function show($id);
   public function edit($id);
   public function update($array);
   public function destroy();
   public function profile();
   public function updateprofile($array);
}

?>