<?php

namespace App\Interfaces;
use App\Model\Traineer;
use Illuminate\Support\Collection;

interface TraineerRepositoryInterface
{   
    public function store($array);
    public function show($id);
    public function edit($id);
    public function update($array);
    public function destroy();
    public function profile();
    public function updatepassword($array);
    public function updateprofile($array);

}

?>


