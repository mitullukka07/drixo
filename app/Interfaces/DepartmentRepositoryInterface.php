<?php

namespace App\Interfaces;
use App\Model\Department;
use Illuminate\Support\Collection;

interface DepartmentRepositoryInterface
{   
    public function store(array $array);

    public function show($id);

    public function edit($id);

    public function updatedep($array);

    public function destroy();

}
?>