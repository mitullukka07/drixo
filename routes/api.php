<?php

use App\Http\Controllers\API\DepartmentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\LoginController;
use App\Http\Controllers\API\Task\TaskController;
use App\Http\Controllers\API\Trainee\TraineeController;
use App\Http\Controllers\API\Traineer\TraineerController;
use App\Http\Controllers\API\Traineer\TraineerLoginController;
use App\Http\Controllers\API\Trainee\TraineeLoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login',[LoginController::class,'showLoginForm']);


Route::post('traineerlogin',[TraineerLoginController::class,'TraineerLogin']);

Route::post('traineelogin',[TraineeLoginController::class,'TraineeLogin']);

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('adminLogout',[LoginController::class,'adminLogout']);
    Route::get('traineerLogout',[TraineerController::class,'traineerLogout']);
    
    Route::group(['prefix' => 'department'], function () {
        Route::get('index', [DepartmentController::class, 'index'])->name('index');
        Route::get('show/{id}', [DepartmentController::class, 'show'])->name('show');
        Route::post('store', [DepartmentController::class, 'store'])->name('store');
        Route::post('update', [DepartmentController::class, 'update'])->name('update');
        
    });

    Route::group(['prefix' => 'traineer'], function () {
        Route::get('index', [TraineerController::class, 'index']);
        Route::post('store', [TraineerController::class, 'store']);
        Route::get('show/{id}', [TraineerController::class, 'show']);
        Route::post('update',[TraineerController::class,'update']);
    });

    Route::group(['prefix'=>'trainee'],function(){
        Route::post('store',[TraineeController::class,'store']);
        Route::get('index',[TraineeController::class,'index']);
        Route::get('show/{id}',[TraineeController::class,'show']);
        Route::post('update',[TraineeController::class,'update']);
    });

    Route::group(['prefix'=>'task'],function(){
        Route::post('store',[TaskController::class,'store']);
        Route::get('index',[TaskCOntroller::class,'index']);
        Route::get('show/{id}',[TaskController::class,'show']);
        Route::post('update',[TaskController::class,'update']);
    });
});

Route::group(['middleware' => 'auth:api_traineer'], function () {
    Route::get('traineerLogout',[TraineerLoginController::class,'traineerLogout']);
    
    Route::group(['prefix' => 'traineer'], function () {
        Route::get('index', [TraineerController::class, 'index']);
        Route::post('store', [TraineerController::class, 'store']);
        Route::get('show/{id}', [TraineerController::class, 'show']);
        Route::post('update',[TraineerController::class,'update']);
        Route::get('profile',[TraineerController::class,'profile']);
    });
});


Route::group(['middleware' => 'auth:api_trainee'], function () {
    Route::get('traineeLogout',[TraineeLoginController::class,'traineeLogout']);
    
    Route::group(['prefix'=>'trainee'],function(){
        Route::post('store',[TraineeController::class,'store']);
        Route::get('index',[TraineeController::class,'index']);
        Route::get('show/{id}',[TraineeController::class,'show']);
        Route::post('update',[TraineeController::class,'update']);
    });

    
});

