<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\TraineeController;
use App\Http\Controllers\Admin\Auth\TraineerAuthController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TraineerController;



Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', 'TraineerAuthController@showLoginForm')->name('login');
    Route::post('login', 'TraineerAuthController@login');
    Route::post('traineer/logout', 'TraineerAuthController@logout')->name('traineer.logout');

    Route::get('login/github','TraineerAuthController@redirectToGitHub')->name('login.github');
    Route::get('login/github/callback','TraineerAuthController@handleGitHubCallback');
});

Route::group(['middleware' => 'auth:traineer'], function () {    
    Route::get('/traineerdashboard', function () {
        return view('admin.traineerdashboard');
    })->name('traineerdashboard');

    Route::group(['prefix'=>'department'],function(){    
        Route::get('index',[DepartmentController::class,'index'])->name('department.index');
    }); 

     Route::group(['prefix'=>'trainee'],function(){    
        Route::get('index',[TraineeController::class,'index2'])->name('trainee.index');
    });   
    
    Route::group(['prefix'=>'traineer'],function(){
        Route::get('index',[TraineerController::class,'index'])->name('traineer.index');
        Route::get('profile',[TraineerController::class,'profile'])->name('traineer.profile');
        Route::post('profile',[TraineerController::class,'updateprofile']);
        Route::get('changepassword',[TraineerController::class,'changepassword'])->name('traineer.changepassword');
        Route::post('changepassword',[TraineerController::class,'updatepassword']);
    });

    Route::group(['prefix'=>'task'],function(){
        Route::get('index',[TaskController::class,'index2'])->name('task.index');        
    });
});

?>

  

