<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\TraineeController;
use App\Http\Controllers\TraineerController;
use App\Http\Controllers\TaskController;



Route::group(['namespace' => 'Auth'], function () {
    # Login Routes
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('admin/logout', 'LoginController@logout')->name('logout');

    Route::get('login/google','LoginController@redirectToGoogle')->name('login.google')->where('social','google');
    Route::get('login/google/callback','LoginController@handleGoogleCallback')->where('social','google');
});

Route::group(['middleware' => 'auth:admin'], function () {
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');


    Route::group(['prefix'=>'department'],function(){
        Route::get('create',[DepartmentController::class,'create'])->name('create');
        Route::post('store',[DepartmentController::class,'store'])->name('department.store');
        Route::get('index',[DepartmentController::class,'index'])->name('department.index');
        Route::get('edit/{id}',[DepartmentController::class,'edit'])->name('department.edit');
        Route::post('update',[DepartmentController::class,'update'])->name('department.update');
        Route::get('show/{id}',[DepartmentController::class,'show'])->name('department.show');
    });
    
    Route::group(['prefix'=>'trainee'],function(){
        Route::get('create',[TraineeController::class,'create'])->name('trainee.create');
        Route::post('store',[TraineeController::class,'store'])->name('trainee.store');
        Route::get('index',[TraineeController::class,'index'])->name('trainee.index');
        Route::get('edit/{id}',[TraineeController::class,'edit'])->name('trainee.edit');
        Route::post('update',[TraineeController::class,'update'])->name('trainee.update');
        Route::get('show/{id}',[TraineeController::class,'show'])->name('trainee.show');      
    });
    
    Route::group(['prefix'=>'traineer'],function(){
        Route::get('create',[TraineerController::class,'create'])->name('traineer.create');
        Route::post('store',[TraineerController::class,'store'])->name('traineer.store');
        Route::get('index',[TraineerController::class,'index2'])->name('traineer.index');
        Route::get('edit/{id}',[TraineerController::class,'edit'])->name('traineer.edit');
        Route::post('update',[TraineerController::class,'update'])->name('traineer.update');
        Route::get('show/{id}',[TraineerController::class,'show'])->name('traineer.show');
    });

    Route::group(['prefix'=>'task'],function(){
        Route::get('create',[TaskController::class,'create'])->name('task.create');
        Route::post('store',[TaskController::class,'store'])->name('task.store');
        Route::get('index',[TaskController::class,'index'])->name('task.index');
        Route::get('edit/{id}',[TaskController::class,'edit'])->name('task.edit');
        Route::get('show/{id}',[TaskController::class,'show'])->name('task.show');
        Route::post('update',[TaskController::class,'update'])->name('task.update');
        Route::get('changeStatus',[TaskController::class,'changeStatus'])->name('task.changeStatus');
    });
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('profile',[AdminController::class,'profile'])->name('admin.profile');
    Route::post('profile',[AdminController::class,'updateprofile']);
    Route::get('changepassword', [AdminController::class, 'changepassword'])->name('changepassword');
    Route::post('changepassword', [AdminController::class, 'updatepassword']);
});

?>

