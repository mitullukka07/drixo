<?php

use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\TraineeController;
use App\Http\Controllers\TraineerController;
use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\Auth\AdminAuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::group(['prefix'=>'department'],function(){
//     Route::get('create',[DepartmentController::class,'create'])->name('create');
//     Route::post('store',[DepartmentController::class,'store'])->name('department.store');
//     Route::get('index',[DepartmentController::class,'index'])->name('department.index');
//     Route::get('edit/{id}',[DepartmentController::class,'edit'])->name('department.edit');
//     Route::post('update',[DepartmentController::class,'update'])->name('department.update');
// });

// Route::group(['prefix'=>'trainee'],function(){
//     Route::get('create',[TraineeController::class,'create'])->name('trainee.create');
//     Route::post('store',[TraineeController::class,'store'])->name('trainee.store');
//     Route::get('index',[TraineeController::class,'index'])->name('trainee.index');
//     Route::get('edit/{id}',[TraineeController::class,'edit'])->name('trainee.edit');
//     Route::post('update',[TraineeController::class,'update'])->name('trainee.update');
// });

// Route::group(['prefix'=>'traineer'],function(){
//     Route::get('create',[TraineerController::class,'create'])->name('traineer.create');
//     Route::post('store',[TraineerController::class,'store'])->name('traineer.store');
//     Route::get('index',[TraineerController::class,'index'])->name('traineer.index');
//     Route::get('edit/{id}',[TraineerController::class,'edit'])->name('traineer.edit');
//     Route::post('update',[TraineerController::class,'update'])->name('traineer.update');
// });
