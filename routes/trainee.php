<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\TraineeController;
use App\Http\Controllers\Admin\Auth\TraineerAuthController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TraineerController;


Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', 'TraineeAuthController@showLoginForm')->name('login');
    Route::post('login', 'TraineeAuthController@login');
    Route::post('trainee/logout', 'TraineeAuthController@logout')->name('trainee.logout');

    Route::get('login/linkedin','TraineeAuthController@redirectToLinkedin')->name('login.linkedin');
    Route::get('login/linkedin/callback','TraineeAuthController@handleLinkedinCallback');
});

Route::group(['middleware' => 'auth:trainee'], function () {    
    Route::get('/traineedashbord', function () {
        return view('admin.traineedashboard');
    })->name('traineedashbord');

    Route::group(['prefix'=>'trainee'],function(){
        Route::get('index',[TraineeController::class,'index2'])->name('trainee.index');
        Route::get('index',[TraineeController::class,'index3'])->name('trainee.index');
        Route::get('profile',[TraineeController::class,'profile'])->name('trainee.profile');
        Route::post('profile',[TraineeController::class,'updateprofile']);
        Route::get('changepassword',[TraineeController::class,'changepassword'])->name('traineer.changepassword');
        Route::post('changepassword',[TraineeController::class,'updatepassword']);
    });

    Route::group(['prefix'=>'traineer'],function(){
        Route::get('index',[TraineerController::class,'index3'])->name('traineer.index');        
    });
   
    Route::group(['prefix'=>'task'],function(){
        Route::get('index',[TaskController::class,'index3'])->name('task.index');        
    });

});


?>